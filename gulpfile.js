/**
This is bloody crazy, to make a build work, no matter which tool you choose you lose.
So we use tools inside tools and add hacks to it so it works, the javascript ecosystem is aWesOMe!!!
**/

const { src, dest, series, parallel } = require('gulp');
const rollup = require('rollup');

const typescript = require('rollup-plugin-typescript2');
const nodeResolve = require('rollup-plugin-node-resolve');
const commonjs = require('rollup-plugin-commonjs');
//import css from 'rollup-plugin-css-only'
const { terser } = require('rollup-plugin-terser');
const purgecss = require('gulp-purgecss');
const json = require('rollup-plugin-json');
const jeditor = require("gulp-json-editor");
const semver = require('semver')
const zipFolder = require('zip-folder');
const git = require('simple-git/promise')();
const replace = require('gulp-string-replace');
const inquirer = require('inquirer');
const clean = require('gulp-clean');
const out = "dist"

// default build stuffies

function cleanDist() {
  return src(out, {read: false})
        .pipe(clean());
}

async function buildBackground() {
  const bundle = await rollup.rollup({
    input: 'browser-ext/src/bg/background.ts',
    plugins: [
      json(), nodeResolve(), commonjs(), typescript(), terser()
    ]
  });
  await bundle.write({
    file: out + '/src/bg/background.js',
    format: 'esm',
    sourcemap: false
  });
}

async function buildPages() {
  const bundle = await rollup.rollup({
    input: 'browser-ext/src/pages/pages.ts',
    plugins: [
      nodeResolve(), commonjs(), typescript(), terser()
    ]
  });
  await bundle.write({
    file: out + '/src/pages/pages.js',
    format: 'esm',
    sourcemap: false
  });
}

async function buildInject() {
  const bundle = await rollup.rollup({
    input: 'browser-ext/src/inject/inject_vis.js',
    plugins: [
      nodeResolve(), commonjs(), terser()
    ]
  });
  const bundle2 = await rollup.rollup({
    input: 'browser-ext/src/inject/inject_ws.js',
    plugins: [
      nodeResolve(), commonjs(), terser()
    ]
  });
  
  await bundle.write({
    file: out + '/src/inject/inject_vis.js',
    format: 'esm',
    sourcemap: false
  });
  await bundle2.write({
    file: out + '/src/inject/inject_ws.js',
    format: 'esm',
    sourcemap: false
  });
}


function buildCss() {
  return src('browser-ext/src/pages/style.css')
    .pipe(
      purgecss({
        content: ['browser-ext/src/pages/*.html']
      })
    )
    .pipe(dest(out + "/src/pages/"))
}

function copyRemainingFiles() {
  return src([
    'browser-ext/**/*.html', 
    'browser-ext/*.json', 
    'browser-ext/**/*.png'
     //, 'browser-ext/**/override/*.*'
    ])
    .pipe(dest(out));
}

// Create new release tag on master

async function updateVersion() {
  //git.merge('dev', function (err) {
  //  if (err) throw err;
  //}); 

  let oldTag = (await git.raw(["describe", "--abbrev=0", "--tags"])).trim()
  let newTag = semver.inc(oldTag, 'patch')

  let message = (await inquirer.prompt([{
    type: "input",
    name: "u",
    message: `Update from ${oldTag} to v${newTag}. Give version summary:`
  }]))["u"]

  await src("browser-ext/manifest.json")
    .pipe(jeditor(function (json) {
      json.version = newTag
      return json;
    }))
    .pipe(dest("browser-ext"));  


  await src("./README.md").pipe(replace(new RegExp(`.*${oldTag}.*`, "g"), (result) => {
    return `${result}\r\n- v${newTag}: ${message}`
  })).pipe(dest("./"));  

  await git.raw(["add", "-A"])

  await git.commit(`Version update to ${newTag} - ${message}`)
  
  await git.addTag('v' + newTag) 

  await git.checkout("master")

  await git.merge(["dev"])

  await git.checkout("dev")

}

// Chrome build


function cleanChromeDist() {
  return src("dist.chrome", {read: false, allowEmpty: true}).pipe(clean())
}

function moveBuild() {
  return src(['dist/**']).pipe(dest('dist.chrome'));
}

function removeKeyFromManifest() {
  return src("dist.chrome/manifest.json")
    .pipe(jeditor(function (json) {
      delete json.key;
      return json;
    }))
    .pipe(dest("dist.chrome"));
}

async function publishToChromeStore() {
  let tag = (await git.raw(["describe", "--abbrev=0", "--tags"])).trim()
  await new Promise(function(resolve) {
    zipFolder("dist.chrome", "dist.chrome."+tag+".zip", function (err) {
    if (err) {
      console.log('oh no! ', err);
      resolve()
    } else {
      //console.log(`Successfully zipped the ${folderName} directory and store as ${zipName}`);
      // will be invoking upload process 
      resolve()
    }
  });
  })
  await src("dist.chrome", {read: false}).pipe(clean());
  
  
}

exports.default = series(
  cleanDist,
  parallel(buildBackground, buildPages, buildInject, copyRemainingFiles, buildCss)
)
exports.updateVersion = series(
  exports.default,
  updateVersion
)
exports.buildChrome = series(
  exports.default,
  cleanChromeDist,
  moveBuild,
  removeKeyFromManifest,
  publishToChromeStore
)

