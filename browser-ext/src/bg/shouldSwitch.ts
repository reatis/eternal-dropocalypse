import {level, levelRank} from "./util"
import moment from 'moment';
 
export function shouldSwitch(
    CONFIG: IConfig,
    watchingNow: IInternalCampaign | null,
    knownCampaigns: IJsonCampaign[],
    offeredCampaign: IJsonCampaign,
    accMinutes: IAccCampaigns,
    hoursPastInWindow: number,
    currentlyWatchingInChrome: string): boolean { 
    const offeredType = level(offeredCampaign)
    const offeredTypeRank = levelRank(offeredType)

    function stretchesStillToBeFilled(type: string) {
      if(type == "silver" || type == "gold" || type == "diamond") {
        const seenTop = accMinutes.silver.nonStopStretches + accMinutes.gold.nonStopStretches + accMinutes.diamond.nonStopStretches
        const topAvailable = Math.max(0, 4 - seenTop)
        return  Math.min(topAvailable, Math.max(0, 2 - accMinutes[type].nonStopStretches))
      } else {
        return  Math.max(0, 2 - accMinutes[type].nonStopStretches)
      }     
    }

    const offeredLogData = `offered: ${offeredType} by ${offeredCampaign.twitchUser} watching: ${watchingNow ? watchingNow.user : "nothing"}`

    function shouldWatchBasedOnTime() {
      if (stretchesStillToBeFilled(offeredType) == 0) {
        return false
      }  
      if (offeredType == "silver") {
        if (CONFIG.silver == "smart") {
          console.log(`Smart silver algo - hours past: ${hoursPastInWindow}`)
          if (hoursPastInWindow > 5 && (accMinutes.gold.nonStopStretches == 0 && accMinutes.diamond.nonStopStretches == 0)) {
            // no gold AND diamond ran yet, room for 2 high campaigns
            return  stretchesStillToBeFilled(offeredType) > 0
          } else if (hoursPastInWindow > 11 && (accMinutes.gold.nonStopStretches == 0 || accMinutes.diamond.nonStopStretches == 0)) {
            // no gold OR diamond ran yet, room for 1 high campaigns
            return  stretchesStillToBeFilled(offeredType) > 0
          } else if (hoursPastInWindow > 17) {
            // default
            return  stretchesStillToBeFilled(offeredType) > 0
          } else {
            return false;
          }
        } else if (CONFIG.silver == "never") {
          console.log("Never silver algo")
          return false
        } else {
          console.log("Always silver algo")
          return stretchesStillToBeFilled(offeredType) > 0
        }
      } else {
        // still need atleast 1 stretch
        return stretchesStillToBeFilled(offeredType) > 0
      }
    }
  
    // if not watching now
    if (!watchingNow) {
      console.log(`shouldSwitch (${offeredLogData}): Not following a campaign, decision based on needed campaigns`)
      return shouldWatchBasedOnTime()
    } else {
      const currentType = watchingNow.type
      const currentTypeRank = levelRank(currentType)
      const minutesFollowed = (watchingNow.end - watchingNow.start) / 1000 / 60
      const minutesRemaining = (watchingNow.campaignEnd - moment.utc().valueOf()) / 1000 / 60

      console.log(`shouldSwitch (${offeredLogData}): Following a campaign (${watchingNow.user} chrome ${currentlyWatchingInChrome}), decision based on both current and needed campaigns`)
  
      if (currentTypeRank == offeredTypeRank) {        
        //console.log(`Test offeredCamaign.twitchUser [${offeredCamaign.twitchUser}] == currentlyWatchingInChrome [${currentlyWatchingInChrome}] : ${offeredCamaign.twitchUser == currentlyWatchingInChrome}`)
        if(stretchesStillToBeFilled(offeredType) == 0) {
          // Don't bother if already seen enough          
          console.log(`shouldSwitch (${offeredLogData}): Same rank offered, but rank already fulfilled, don't follow`)
          return false
        }
        if (offeredCampaign.twitchUser != currentlyWatchingInChrome) {          
          const strechesNeeded = stretchesStillToBeFilled(offeredType)          
          if (minutesFollowed + minutesRemaining > 177) {
            //new user, but possibility to fill up minutes with current campaign
            console.log(`shouldSwitch (${offeredLogData}): Same rank, different user (${offeredCampaign.twitchUser} instead of current ${currentlyWatchingInChrome}) - Can get enough time (2 stretches, need ${strechesNeeded}) out of current (177), staying`)
            return false
          } else if (minutesFollowed + minutesRemaining > 90 && strechesNeeded == 1) {
            console.log(`shouldSwitch (${offeredLogData}): Same rank, different user (${offeredCampaign.twitchUser} instead of current ${currentlyWatchingInChrome}) - Can get enough time (1 stretch, need ${strechesNeeded}) out of current (90), staying`)
          } {
            // be opportunistic, move to longer running campaign
            console.log(`shouldSwitch (${offeredLogData}): Same rank, different user (${offeredCampaign.twitchUser} instead of current ${currentlyWatchingInChrome}) - Decided be whether new campaign has more time left ${offeredCampaign.minutesLeft > minutesRemaining}`)
            return offeredCampaign.minutesLeft > minutesRemaining
          }
        } else {
          // same user, we stay
          if(offeredType == "silver" && (CONFIG.silver == "never" || CONFIG.silver == "smart")) {
            console.log(`shouldSwitch (${offeredLogData}): Same rank (SILVER), same user, deciding based on time and whether to watch silver`)
            return shouldWatchBasedOnTime()
          } else {
            // keep watching current user even if not needed
            console.log(`shouldSwitch (${offeredLogData}): Same rank, same user, staying`)
            return true
          }          
        }
      } else if (currentTypeRank > offeredTypeRank && stretchesStillToBeFilled(currentType) > 0 && watchingNow.campaignEnd > moment.utc().valueOf() - 1000 * 60) {
        /// offered 1) lesser rank and 2) need current rank and 3) current not ended (end lies in future)
        console.log(`shouldSwitch (${offeredLogData}): Currently watching a ${currentType} campaign that still needs watching, offered a ${offeredType}, no thanks`)
        return false
      } else {
        console.log(`shouldSwitch (${offeredLogData}): Different rank (watching: ${currentType} offered ${offeredType}), deciding based on time/need`)
        return shouldWatchBasedOnTime()
      }
    }
  }