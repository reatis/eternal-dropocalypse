import moment from 'moment';
import 'moment-timezone';

import './logging'

import { level, levelRank } from "./util"
import { shouldSwitch } from "./shouldSwitch"
import { getConfigAndListen } from "../config"

const idleUrl = chrome.extension.getURL("src/pages/drops.html?idle")
const configUrl = chrome.extension.getURL("src/pages/drops.html?config")

let CONFIG: IConfig

let IS_INIT = true;

function checkPermissions() {
  chrome.permissions.contains({
    permissions: ["idle", "notifications", "background", "tabs", "storage", "webRequest", "webRequestBlocking", "alarms"],
    origins: ["https://*.twitch.tv/*", "https://twitch.tv/*", "https://*.ext-twitch.tv/*", "https://eternalreplay.com/twitch/campaigns"]
  }, async function (result) {
    if (result) {
      // permission ok
      console.log(`PERMISSIONS GRANTED, STARTING`)

      chrome.webRequest.onHeadersReceived.addListener(
        patchCSP,
        {
          urls: [
            "https://*.ext-twitch.tv/*viewer*"
          ]
        },
        ["blocking", "responseHeaders"]
      );


     /*
      chrome.webRequest.onBeforeRequest.addListener(function (details) {
        return { redirectUrl:  chrome.extension.getURL("src/override/wasmworker.js") };
      }, {
          urls: ['https://cvp.twitch.tv/2.9.2/wasmworker.min.js']
        }, [
          'blocking'
        ]);
      */
     


      /* 
      Stats are fun. let's store them
      */
      chrome.storage.local.get(['dropCountStart'], function (result) {
        if (!result['dropCountStart']) {
          chrome.storage.local.set({ 'dropCountStart': moment().unix() }, function () {
            console.log("Initializing counter")
          });
        } else {
          console.log("Counter started at " + result['dropCountStart'])
        }
      });

      IS_INIT = false; 
      /*
      chrome.alarms.create("madness", { when: Date.now(), periodInMinutes: 1 })
      chrome.alarms.onAlarm.addListener(async (a) => {
        CONFIG = await getConfigAndListen(function (cfg) {
          CONFIG = cfg
        })
        console.log(`Running dropocalypse with config: ${JSON.stringify(CONFIG)}`)
        if(CONFIG.reEnableAfter != "" && !CONFIG.enabled) {
          if (moment().isAfter(moment(CONFIG.reEnableAfter, moment.ISO_8601))) {
            chrome.storage.local.get(["extension-config"], function (result) {
              let c = result["extension-config"]
              if(c) {
                c.enabled = "yes"
                c.reEnableAfter = ""
                console.log("ReEnabling Extension")
                chrome.storage.local.set({ "extension-config": c })
              }
            })
          }
        }

        if (CONFIG.enabled) {
          let window = await getCampaignWindow();
          window.updateStreamers()
        }
      })
      */

    } else {
      console.log(`NEED PERMISSIONS, ASKING ${IS_INIT}`)
      if (IS_INIT) { chrome.tabs.create({ url: configUrl, active: true }); }
      IS_INIT = false;

    }
  });
}

checkPermissions()

chrome.permissions.onAdded.addListener(checkPermissions)
chrome.permissions.onRemoved.addListener(checkPermissions)

window.addEventListener('error', function (event) {
  console.error(event.error.stack);
}, false);



let WSLOG: { [key: string]: number; } = {}

function updateTwitchSeen(url: string) {
  try {
    let elements = url.split("/")
    const user = elements[elements.length - 1]
    WSLOG[user] = moment.utc().valueOf()
  } catch (e) {

  }
}

let HEALTHLOG: { [key: string]: number; } = {}

function checkHealth(tab: chrome.tabs.Tab) {
  try {
    checkAndForceFocusOfTab(tab, tab.url!)
    let elements = tab.url!.split("/")
    const user = elements[elements.length - 1]
    console.log(`Running healthcheck for ${user}: ${JSON.stringify(WSLOG)} - ${!WSLOG[user]}`)
    if (!WSLOG[user] || WSLOG[user] < moment.utc().valueOf() - 1000 * 60) {
      console.log(`Healthcheck - ${user} has not recieved messages for over a minute, refreshing tab. HEALTHLOG: ${HEALTHLOG[user]}`)
      HEALTHLOG[user] = HEALTHLOG[user] ? HEALTHLOG[user] + 1 : 1
      if (HEALTHLOG[user] <= 5) {
        chrome.tabs.reload(tab.id!)
      } else {
        console.warn("Excessive reloading")
      }
    } else {
      HEALTHLOG[user] = 0
    }
  } catch (e) {
    console.warn(e)
  }
}

function checkAndForceFocusOfTab(tab: chrome.tabs.Tab, url: string) {
  if (tab && tab.url != idleUrl) {
    chrome.tabs.executeScript(tab.id!, { code: "[document.visibilityState, document.hasFocus(), document.hidden, document.readyState, document.webkitVisibilityState, document.webkitHidden]" }, result => {
      const isOk = result[0][0] == "visible" && result[0][1] && !result[0][2]
      if (!tab.active) {
        if (CONFIG.forceFocus) {
          console.log(`Active check failed for ${tab.url} active: ${tab.active} - Patching things up`)
          chrome.tabs.update(tab.id!, { active: true })
        } else {
          console.log(`Active check failed for ${tab.url} active: ${tab.active}`);
        }
      }
      if (!isOk) {
        console.warn(`Focus check failed for ${tab.url} active: ${tab.active} ok: ${isOk} [${JSON.stringify(result)}]. This should not be possible`)
      }
    })
  }
}







class CampaignWindow implements ICampaignWindow {
  time: number;
  campaigns: IInternalCampaign[];
  streamers: IStreamers[];

  constructor(data: ICampaignWindow) {
    //{user, type, start, end}
    this.time = data.time
    this.campaigns = data.campaigns
    this.streamers = data.streamers
    //{user, type, start, end}
  }

  // obsolete
  /*
  getCurrentlyWatching(): IInternalCampaign | null {
    const lastWatched = this.campaigns[this.campaigns.length - 1]
    if (lastWatched && (moment.utc().valueOf() - lastWatched.end) / 1000 / 60 < 5) {
      return lastWatched
    }
    else return null
  }
  */

  // obsolete
  /*
  watch(eitherCJsonOrUser: { cJson?: IJsonCampaign; internalCampaign?: IInternalCampaign }, tab: chrome.tabs.Tab): void {
    let cJson = eitherCJsonOrUser.cJson
    let internalCampaign = eitherCJsonOrUser.internalCampaign


    let now = moment.utc()

    if (cJson) {
      console.log(`Watching from cJson, doing clever stuff`)
      var url = `https://www.twitch.tv/${cJson.twitchUser}`
      if (tab.url != url) {
        chrome.tabs.update(tab.id!, { url: url, active: true }, function () {
          checkAndForceFocusOfTab(tab, url)
        });
        this.campaigns.push({
          user: cJson.twitchUser,
          type: level(cJson),
          start: now.valueOf(),
          end: now.valueOf(),
          campaignEnd: cJson.endTime
        })
      } else {
        const lastWatched = this.campaigns[this.campaigns.length - 1]
        if (lastWatched && lastWatched.user == cJson.twitchUser && lastWatched.type == level(cJson)) {
          console.log("Update end of what is being watched")
          this.campaigns[this.campaigns.length - 1].end = now.valueOf()
        } else {
          this.campaigns.push({
            user: cJson.twitchUser,
            type: level(cJson),
            start: now.valueOf(),
            end: now.valueOf(),
            campaignEnd: cJson.endTime
          })
        }
      }
    }
    if (internalCampaign) {
      console.log(`Watching from internalCampaign, doing less clever stuff`)
      var url = `https://www.twitch.tv/${internalCampaign.user}`
      if (tab.url != url) {
        console.log("OOOPS, watch from lastcampaign, but tabs don't match, should still be on the tab, things broke")
        chrome.tabs.update(tab.id!, { url: url, active: true }, function () {
          checkAndForceFocusOfTab(tab, url)
        });
      } else {
        const lastWatched = this.campaigns[this.campaigns.length - 1]
        if (lastWatched && lastWatched.user == internalCampaign.user && lastWatched.type == internalCampaign.type) {
          console.log("Update end of what is being watched")
          this.campaigns[this.campaigns.length - 1].end = now.valueOf()
        } else {
          console.log("OOOPS, watch from lastcampaign, but last campaign is different wtfbbq")
        }
      }
    }
    //console.log(JSON.stringify(this.campaigns))

    var campaignWindowKey = `campaignWindow-${this.time}`

    var newData: { [key: string]: { time: number; campaigns: IInternalCampaign[] } } = {}
    newData[campaignWindowKey] = {
      time: this.time,
      campaigns: this.campaigns
    }
    chrome.storage.local.set(newData, function () {
      console.log(`Update campaignWindow ${campaignWindowKey} with ${JSON.stringify(newData[campaignWindowKey])}`)
    });
  }
  */

  //obsolete
  /*
  wantToWatch(cJson: IJsonCampaign, knownCampaigns: IJsonCampaign[], currentlyWatchingInChrome: string): boolean {
    let now = moment.utc()
    let seen: IAccCampaigns = {
      bronze: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 },
      silver: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 },
      gold: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 },
      diamond: { totalMinutes: 0, nonStopStretches: 0, distinctUsers: 0 }
    }
    this.campaigns.forEach(c => {
      const minutes = (c.end - c.start) / 1000 / 60
      seen[c.type].totalMinutes += minutes
      if (minutes >= 177) {
        seen[c.type].nonStopStretches += 2
      } else if (minutes > 90) {
        seen[c.type].nonStopStretches += 1
      }

      seen[c.type].distinctUsers += 1
    });
    console.log(JSON.stringify(seen))

    // the highest level campaign always goes first, so no fear of accepting a low level one when there is a choice
    const currentlyWatching = this.getCurrentlyWatching()
    let hoursPastInWindow = now.diff(moment.utc(this.time), "hours")

    return shouldSwitch(CONFIG, currentlyWatching, knownCampaigns, cJson, seen, hoursPastInWindow, currentlyWatchingInChrome)
  }
  */

  //obsolete
  /*
  async update(): Promise<void> {
    let campaignCutoff = this.time

    // get currently following tabs
    let tabs: chrome.tabs.Tab[] = await new Promise(function (resolve, reject) {
      chrome.tabs.query({ "url": ["*://www.twitch.tv/*", idleUrl] }, function (tabs) {
        resolve(tabs)
      });
    })

    console.log(`Tabs managed by extension: ${tabs.length}`)

    // allow only one twitch tab
    if (tabs.length > 1) {
      for (let index = 1; index < tabs.length; index++) {
        chrome.tabs.remove(tabs[index].id!)
      }
    }

    let currentlyWatchingInChrome = ""
    if (tabs.length != 0) {
      if (tabs[0].url != idleUrl) {
        checkHealth(tabs[0])
        let elements = `${tabs[0].url}`.split("/")
        currentlyWatchingInChrome = elements[elements.length - 1]
      }
    }

    let managedTab = tabs[0]
    if (!managedTab) {
      managedTab = await new Promise(function (resolve, reject) {
        chrome.tabs.create({ url: idleUrl }, function (tab) {
          resolve(tab)
        })
      })
    }

    const response = await fetch('https://eternalreplay.com/twitch/campaigns');
    const activeCampaigns = await response.json();

    // order active campaigns by most wanted frst
    activeCampaigns.sort((a: any, b: any) => {
      if (a.campaignType != b.campaignType) {
        //higher campaign level = better
        const types = ["bronze", "silver", "gold", "diamond"]
        const level_a = types.findIndex((t) => { return a.campaignType.toLowerCase().includes(t) })
        const level_b = types.findIndex((t) => { return b.campaignType.toLowerCase().includes(t) })
        return level_b - level_a
      } else {
        // the more time left, the better
        return b.minutesLeft - a.minutesLeft
      }
    })

    console.log(`Watching: ${currentlyWatchingInChrome} - Found campaigns on twitch: ${JSON.stringify(activeCampaigns)}`)

    let foundCampaignToWatch = false;
 
    for (let index = 0; index < activeCampaigns.length; index++) {
      const cJson = activeCampaigns[index];
      if (this.wantToWatch(cJson, activeCampaigns, currentlyWatchingInChrome)) {
        console.log("Want to see: " + JSON.stringify(cJson))
        foundCampaignToWatch = true;
        this.watch({ cJson: cJson }, managedTab)
        break;
      }
    }

    if (!foundCampaignToWatch) {
      const lastWatched = this.campaigns[this.campaigns.length - 1]
      if (lastWatched && lastWatched.campaignEnd > moment.utc().valueOf()) {
        // still watching some camign, keep it open just to be sure as there is nothing better       
        console.log("Nothing needed to watch, but current campaign is still running, let's keep following")
        this.watch({ internalCampaign: lastWatched }, managedTab)
      }
      if (!lastWatched || lastWatched && (moment.utc().valueOf() - lastWatched.end) / 1000 / 60 > 5) {
        console.log("Nothing watched for 5 min, cleaning up")
        if (managedTab && managedTab.url != idleUrl) {
          chrome.tabs.update(managedTab.id!, { url: idleUrl });
        }
      }
    }
  }
  */

 async updateStreamers(): Promise<void> {
  let campaignCutoff = this.time

  const response = await fetch('https://eternalreplay.com/twitch/streams');
  const activeStreams: string[] = await response.json();
  let now = moment.utc()

  let minutesInWindow = 0;
  for (const streamer of this.streamers) {
    minutesInWindow += ((streamer.end - streamer.start) / 1000 / 60)
  }
  
  // get currently following tabs
  let tabs: chrome.tabs.Tab[] = await new Promise(function (resolve, reject) {
    chrome.tabs.query({ "url": ["*://www.twitch.tv/*", idleUrl] }, function (tabs) {
      resolve(tabs)
    });
  })

  console.log(`Tabs managed by extension: ${tabs.length}`)

  let watchingNow = null;
  // allow only one twitch tab
  if (tabs.length > 1) {
    for (let index = 0; index < tabs.length; index++) {
      const tabUrl = tabs[index].url!
      for (const s of activeStreams) {
         if(tabUrl.includes(`twitch.tv.tv/${s}`)) {
          watchingNow = s;
          break;
         }
      }
      if (null != watchingNow) {
        break;
      }     
    }
  }
  if (null == watchingNow && activeStreams.length > 0 && minutesInWindow < CONFIG.minutesPerDay) {
    await new Promise(function (resolve, reject) {
      chrome.tabs.create({ url: idleUrl }, function (tab) {
        resolve(tab)
      })
    })
  }
  let managedTab = tabs[0]
  if (!managedTab) {
    managedTab = await new Promise(function (resolve, reject) {
      chrome.tabs.create({ url: idleUrl }, function (tab) {
        resolve(tab)
      })
    })
  }

 /*

  // order active campaigns by most wanted frst
  activeCampaigns.sort((a: any, b: any) => {
    if (a.campaignType != b.campaignType) {
      //higher campaign level = better
      const types = ["bronze", "silver", "gold", "diamond"]
      const level_a = types.findIndex((t) => { return a.campaignType.toLowerCase().includes(t) })
      const level_b = types.findIndex((t) => { return b.campaignType.toLowerCase().includes(t) })
      return level_b - level_a
    } else {
      // the more time left, the better
      return b.minutesLeft - a.minutesLeft
    }
  })

  console.log(`Watching: ${currentlyWatchingInChrome} - Found campaigns on twitch: ${JSON.stringify(activeCampaigns)}`)

  let foundCampaignToWatch = false;

  for (let index = 0; index < activeCampaigns.length; index++) {
    const cJson = activeCampaigns[index];
    if (this.wantToWatch(cJson, activeCampaigns, currentlyWatchingInChrome)) {
      console.log("Want to see: " + JSON.stringify(cJson))
      foundCampaignToWatch = true;
      this.watch({ cJson: cJson }, managedTab)
      break;
    }
  }

  if (!foundCampaignToWatch) {
    const lastWatched = this.campaigns[this.campaigns.length - 1]
    if (lastWatched && lastWatched.campaignEnd > moment.utc().valueOf()) {
      // still watching some camign, keep it open just to be sure as there is nothing better       
      console.log("Nothing needed to watch, but current campaign is still running, let's keep following")
      this.watch({ internalCampaign: lastWatched }, managedTab)
    }
    if (!lastWatched || lastWatched && (moment.utc().valueOf() - lastWatched.end) / 1000 / 60 > 5) {
      console.log("Nothing watched for 5 min, cleaning up")
      if (managedTab && managedTab.url != idleUrl) {
        chrome.tabs.update(managedTab.id!, { url: idleUrl });
      }
    }
  }

  */
}
}


async function getCampaignWindow(): Promise<CampaignWindow> {
  let now = moment.utc()
  let campaignCutoff = moment.tz("00", "hh", "America/Los_Angeles").utc()
  if (now.isBefore(campaignCutoff)) {
    campaignCutoff.subtract(1, "d")
  }
  //console.log(`Campaign CutOff: ${campaignCutoff.format()}`);
  
  var campaignWindowKey = `campaignWindow-${campaignCutoff.valueOf()}`

  let p: Promise<CampaignWindow> = new Promise(function (resolve, reject) {
    chrome.storage.local.get(null, function (result) {
      let keys = Object.getOwnPropertyNames(result).filter((k) => { return k.includes("campaignWindow") })
      const wantedWindow = keys.findIndex((k) => {
        const s = moment.utc(result[k].time).tz("America/Los_Angeles")
        const e = s.clone().add(1, "d")
        return moment().isBetween(s, e)
      })
      // console.log(`wanted windows ${JSON.stringify(keys)} ${wantedWindow}`)
      if (wantedWindow < 0) {
        var newData: { [key: string]: { time: number; campaigns: any, streamers: any } } = {}
        newData[campaignWindowKey] = {
          time: campaignCutoff.valueOf(),
          campaigns: [],
          streamers: []
        }
        chrome.storage.local.set(newData, function () {
          console.log(`Initializing campaignWindow ${campaignWindowKey} with ${JSON.stringify(newData[campaignWindowKey])}`)
          resolve(new CampaignWindow(newData[campaignWindowKey]))
        });
      } else {
        resolve(new CampaignWindow(result[keys[wantedWindow]]))
      }
    });
  });

  return await p
}

chrome.browserAction.onClicked.addListener(function (tab) {
  chrome.tabs.create({ url: configUrl })
  document.addEventListener("visibilityChange", function () {

  }, false);
});






function patchCSP(requestDetails: chrome.webRequest.WebResponseHeadersDetails) {
  var newHeaders: string[] = []
  requestDetails.responseHeaders!.forEach((element: any) => {
    if (element.name == "content-security-policy") {
      element.value = element.value.replace("script-src 'self'", "script-src 'self' 'unsafe-inline'")
      newHeaders.push(element)
    } else {
      newHeaders.push(element)
    }
  });
  return { responseHeaders: newHeaders };
}



function countDrop(accepted: boolean, amount: number) {
  var m = moment()
  var groupKey = `dropDay.${m.year()}.${m.dayOfYear()}`
  var data = {
    'unix': m.unix(),
    'accepted': accepted,
    'amount': amount
  }
  chrome.storage.local.get([groupKey], function (result) {
    if (!result[groupKey]) {
      var newData: any = {}
      newData[groupKey] = [data]
      chrome.storage.local.set(newData, function () {
        console.log(`Initializing daily accumulator for ${groupKey} with ${JSON.stringify([data])}`)
      });
    } else {
      result[groupKey].push(data)
      chrome.storage.local.set(result, function () {
        console.log(`Pusshed data to daily accumulator for ${groupKey}. It now holds ${JSON.stringify(result[groupKey])}`)
      });
    }
  });
}

/*
Our listener
 */
let canAcceptDrops = true;

function openChests() {
  if (CONFIG.chests) {
    function test() {
      try {
        const window = CONFIG.chestWindow.map((s) => { return moment(s, "HH:mm") })
        if (window[0].isAfter(window[1])) {
          // thing like 19:00 - 05:00, this is extra interesting
          return moment().isAfter(window[0]) || moment().isBefore(window[1])
        } else {
          // thing like 08:00 - 21:00, no mind bending needed
          return moment().isAfter(window[0]) && moment().isBefore(window[1])
        }
      } catch (e) {
        console.warn("Failure parsing chest window", e)
        return false
      }
    }
    const r = test()
    console.log(`Checking chests in ${JSON.stringify(CONFIG.chestWindow)} at ${moment().format("HH:mm")} => ${r}`)
    return r
  } else {
    return false
  }
}

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  //console.log(sender.tab ? "MSG from a content script:" + sender.tab.url : "MSG from the extension");  
  if (request.t == "GotDrop") {
    console.log(`Got drop: ${sender.tab!.url} ${JSON.stringify(request.dropEvent)} - ${openChests() ? "Trying to claim chest" : "Ignoring chests"} - canAccept: ${canAcceptDrops}`)
    if (canAcceptDrops && openChests()) {
      const randomFactor = Math.floor(Math.random() * 100);
      if (randomFactor > 20) {
        countDrop(true, request.dropEvent.currencyGranted)
        var opt = {
          type: "basic",
          title: "Drop!",
          message: `You recieved ${request.dropEvent.currencyGranted} blue thingies!`,
          iconUrl: "/icons/icon128.png",
          silent: true
        }
        chrome.notifications.create(opt, function (id) {
          setTimeout(() => { chrome.notifications.clear(id) }, 1000 * 15)
        })
        //
        var takeDrop = {
          "nonce": request.dropEvent.nonce,
          "twitchId": request.dropEvent.twitchId,
          "accountId": request.dropEvent.accountId,
          "messageType": "DropAccepted"
        }
        sendResponse(takeDrop)
      } else {
        var opt = {
          type: "basic",
          title: "Missed Drop!",
          message: `You were offered ${request.dropEvent.currencyGranted} blue thingies. Unfortunately you were afk.`,
          iconUrl: "/icons/icon128.png",
          silent: true
        }
        chrome.notifications.create(opt, function (id) {
          setTimeout(() => { chrome.notifications.clear(id) }, 1000 * 15)
        })
      }
      canAcceptDrops = false
      setTimeout(function () { canAcceptDrops = true }, 1000 * 60)
    } else {
      console.log(`Drop from ${sender.tab!.url} is seen as duplicate, not processed`)
      setTimeout(function () { canAcceptDrops = true }, 1000 * 60)
    }
  } else if (request.t == "MiscWsFrame") {
    // console.log(`Misc frame from: ${sender.tab.url}`)
    updateTwitchSeen(sender.tab!.url!)
    sendResponse()
  } else if (request.t == "DropProblem" && openChests() && canAcceptDrops) {
    var opt = {
      type: "basic",
      title: "Drop Problem",
      message: request.data.problem as string,
      iconUrl: "/icons/icon128.png",
      silent: true
    }
    chrome.notifications.create(opt, function (id) {
      setTimeout(() => { chrome.notifications.clear(id) }, 1000 * 15)
    })
    canAcceptDrops = false
    setTimeout(function () { canAcceptDrops = true }, 1000 * 60)
  }
});