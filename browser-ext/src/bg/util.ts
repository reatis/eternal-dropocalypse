export function level(d: IJsonCampaign): string {
  const types = ["bronze", "silver", "gold", "diamond"]
  const level = types.findIndex((t) => { return d.campaignType.toLowerCase().includes(t) })
  return types[level]
}

export function levelRank(d: string): number {
  const types = ["bronze", "silver", "gold", "diamond"]
  return types.findIndex((t) => { return d == t })
}
 

