import tinybind from 'tinybind'
import moment from 'moment';
import WatchJS from 'melanke-watchjs';
import { getConfigAndListenFlat, setConfigFlat } from "../config"
import { async } from 'q';

export default function () {

    chrome.permissions.contains({
        permissions: ["idle", "notifications", "background", "tabs", "storage", "webRequest", "webRequestBlocking", "alarms"],
        origins: ["https://*.twitch.tv/*", "https://twitch.tv/*", "https://*.ext-twitch.tv/*", "https://eternalreplay.com/twitch/campaigns"]
    }, function (result) {
        if (result) {
            document.getElementById('grant')!.style.display = "none"
        } else {
            document.getElementById('grant')!.style.display = "block"
        }
    })

    document.querySelector('#grant a')!.addEventListener('click', function (event) {
        chrome.permissions.request({
            permissions: ["idle", "notifications", "background", "tabs", "storage", "webRequest", "webRequestBlocking", "alarms"],
            origins: ["https://*.twitch.tv/*", "https://twitch.tv/*", "https://*.ext-twitch.tv/*", "https://eternalreplay.com/twitch/campaigns"]
        }, function (granted) {
            // The callback argument will be true if the user granted the permissions.
            if (granted) {
                document.location = document.location
            } else {
                //doSomethingElse();
            }
        });
    })


    if (chrome.storage) {
        tinybind.formatters.isNo = (v: string) => {
            return v == "no"
        }
        tinybind.formatters.isYes = (v: string) => {
            return v == "yes"
        }
        tinybind.formatters.doReEnable = (v: string) => {
            return v != ""
        }
        tinybind.formatters.timeToTimestamp = {
            read: function (v: string) {
                let m = moment(v, moment.ISO_8601)

                if (m.isValid()) {
                    return m.format("HH:mm")
                } else {
                    return ""
                }

            },
            publish: function (v: string) {
                let m = moment(v, "HH:mm")

                if (m.isValid()) {
                    if (m.isBefore(moment())) {
                        m.add(1, "day")
                    }
                    console.log(m.format())
                    return String(m.format())
                } else {
                    return ""
                }
            }
        }




        chrome.storage.local.get(null, function (result) {
            // camaigns
            let keys = Object.getOwnPropertyNames(result).filter((k) => { return k.includes("campaignWindow") })
            let campaignWindows: any = []
            let ct = 0;
            keys.reverse().forEach((k) => {
                let cw = result[k]
                let friendlyTime = moment.utc(cw.time)

                if (ct++ <= 7) {
                    campaignWindows.push({
                        cw: friendlyTime.local().format("dddd, MMMM Do YYYY"),
                        watched: cw.campaigns.map((c: any) => {
                            return {
                                streamer: c.user,
                                type: c.type,
                                minutes: Math.round(0 + (c.end - c.start) / 1000 / 60),
                                start: moment.utc(c.start).local().format("HH:mm")
                            }
                        })
                    })
                }
            })

            let keysChests = Object.getOwnPropertyNames(result).filter((k) => { return k.includes("dropDay") })
            let chests: any = []
            keysChests.reverse().forEach((k) => {
                result[k].forEach((cd: any) => {
                    chests.push ({
                    u: cd.unix,
                    t: moment.unix(cd.unix).format("dddd, MMMM Do YYYY"),
                    amount: cd.amount,
                    accepted: cd.accepted

                })           
                })
                     
            })

            let chestMapped = chests.reduce((acc: any, obj: any) => {
                const key = obj.t
                if (!acc[key]) {
                    acc[key] = {
                        u: obj.u,
                        t: obj.t,
                        d: [],
                        amount: 0,
                        ct: 0
                    };
                  } 
                acc[key].d.push(obj),
                acc[key].amount += obj.accepted ? obj.amount : 0
                acc[key].ct += obj.accepted ? 1 : 0
                return acc;
            }, {})


         
            tinybind.bind(document.getElementById("campaigns"), {
                campaignWindows: campaignWindows,
            })

            let c = Array.from(new Map(Object.entries(chestMapped)).values()).sort((a: any, b: any) => {return b.u - a.u}).slice(0,14)

            tinybind.bind(document.getElementById("chests"), {
                chests:c
            })
        });

        if (document.location.href.indexOf("?config") >= 0) {
            let el = document.getElementById("message")
            if (el) { el.parentNode!.removeChild(el) }
        }



        const watch = WatchJS.watch;

        new Promise(async function (resolve) {
            let config = await getConfigAndListenFlat(function (flat) {
                config = flat
            })
            tinybind.bind(document.getElementById("config"), { config: config })

            // this is dumb as fuck, but appearantly there are no light 2 way databinding libraries like knockout, only full fledged frameworks.
            // knockout is, of course, amazing, unfortunately it uses unsafe methods and the safe patch is not working with typescript
            // seems tinybind is as good as it gets, unfortunately it breaks once you observe the object for changes, thus good old polling
            let change_watcher_hack = Object.assign({}, config)
            setInterval(function () {
                Object.assign(change_watcher_hack, config)
            }, 200)

            watch(change_watcher_hack, function () {
                console.log(`Changes, beautiful changes ${JSON.stringify(config)}`);
                setConfigFlat(config)
            });
            resolve()

        }).then((r) => console.log('config initialized'))



    }
}