const actualCode = `
Document.prototype.hasFocus = function() {return true}
Object.defineProperty(document, 'visibilityState', {
  get: function () { return 'visible'; },
  set: function(value) { }
});
Object.defineProperty(document, 'webkitVisibilityState', {
  get: function () { return 'visible'; },
  set: function(value) { }
});
Object.defineProperty(document, 'hidden', {
  get: function () { return false; },
  set: function(value) { }
});
Object.defineProperty(document, 'webkitHidden', {
  get: function () { return false; },
  set: function(value) { }
});
document.dispatchEvent(new Event("visibilitychange"));`

console.log(`VIS INIT ${document.readyState}`)

document.onreadystatechange = function () {
	if (document.readyState === "interactive") {
		var script = document.createElement('script');
		script.textContent = actualCode;
		(document.head || document.documentElement).appendChild(script);
		script.remove();

		Document.prototype.hasFocus = function() {return true}
		Object.defineProperty(document, 'visibilityState', {
		get: function () { return 'visible'; },
		set: function(value) { }
		});
		Object.defineProperty(document, 'webkitVisibilityState', {
		get: function () { return 'visible'; },
		set: function(value) { }
		});
		Object.defineProperty(document, 'hidden', {
		get: function () { return false; },
		set: function(value) { }
		});
		Object.defineProperty(document, 'webkitHidden', {
		get: function () { return false; },
		set: function(value) { }
		});
		document.dispatchEvent(new Event("visibilitychange"));
	}
}
