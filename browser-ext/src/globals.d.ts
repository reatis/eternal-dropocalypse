interface IJsonCampaign {
    campaignType: string;
    endTime: number;
    endUTC: string;
    minutesLeft: number;
    twitchUser: string;
  }
  
  interface IInternalCampaign {
    user: string;
    type: string;
    start: number;
    end: number;
    campaignEnd: number;
  }

  interface IAccData {
    totalMinutes: number;
    nonStopStretches: number;
    distinctUsers: number;
  }
  
  interface IAccCampaigns {
    [id: string]:IAccData;
    bronze: IAccData;
    silver: IAccData;
    gold: IAccData;
    diamond: IAccData;    
  }
  
  interface IStreamers {
    user: string,
    start: number;
    end: number;
  }

  interface ICampaignWindow {
    time: number;
    campaigns: IInternalCampaign[];
    streamers: IStreamers[];
  }
  
  interface IConfig {
    chests: boolean;
    enabled: boolean;
    forceFocus: boolean;
    silver: string;
    chestWindow: string[];
    reEnableAfter: string;
    fullStretch: number;
    minutesPerDay: number;
  }

  declare enum SilverChoice {
    NEVER = "never",
    ALWAYS = "always", 
    SMART = "smart",
  }

  declare module 'tinybind'
  declare module 'melanke-watchjs';
  declare module 'twitch-js'
  //declare module 'moment';