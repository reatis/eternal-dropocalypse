type FlatConfig = {
    chests: string;
    enabled: string;
    forceFocus: string;
    silver: string;
    chestsFrom: string;
    chestsTo: string;
    reEnableAfter: string;
    fullStretch: number | string;
    minutesPerDay: number | string
};


function configFromFlat(flat: FlatConfig):IConfig {
    return {
        chests: flat.chests == "yes",
        enabled: flat.enabled == "yes",
        forceFocus: flat.forceFocus == "yes",
        silver: flat.silver,
        chestWindow: [flat.chestsFrom, flat.chestsTo],
        reEnableAfter: flat.reEnableAfter,
        fullStretch: +flat.fullStretch,
        minutesPerDay: +flat.minutesPerDay,
      }
}

export async function getConfigAndListenFlat(cb: (n: FlatConfig) => any): Promise<FlatConfig> {
    return new Promise(function (resolve) {
        chrome.storage.local.get(["extension-config"], function (result) {
            let config: FlatConfig = {
                chests: "no",
                enabled: "yes",
                forceFocus: "no",
                silver: "smart",
                chestsFrom: "07:30",
                chestsTo: "21:00",
                reEnableAfter: "",
                fullStretch: 177,
                minutesPerDay: 4 * 90
            }

            if (result["extension-config"]) {
                config.chests = result["extension-config"].chests || config.chests
                config.enabled = result["extension-config"].enabled || config.enabled
                config.forceFocus = result["extension-config"].forceFocus || config.forceFocus
                config.silver = result["extension-config"].silver || config.silver
                config.chestsFrom = result["extension-config"].chestsFrom || config.chestsFrom
                config.chestsTo = result["extension-config"].chestsTo || config.chestsTo
                config.reEnableAfter = result["extension-config"].reEnableAfter || config.reEnableAfter
                config.fullStretch = result["extension-config"].fullStretch || config.fullStretch
                config.minutesPerDay = result["extension-config"].minutesPerDay || config.minutesPerDay
            }

            resolve(config)

            chrome.storage.onChanged.addListener(function (changes, area) {
                if (area == "local" && changes["extension-config"]) {
                    const newValue = changes["extension-config"].newValue
                    console.log(`Onchange fired on cinfig: ${JSON.stringify(newValue)}`);
                    config.chests = newValue.chests
                    config.enabled = newValue.enabled
                    config.forceFocus = newValue.forceFocus
                    config.silver = newValue.silver
                    config.chestsFrom = newValue.chestsFrom
                    config.chestsTo = newValue.chestsTo
                    config.fullStretch = newValue.fullStretch
                    config.minutesPerDay = newValue.minutesPerDay

                    cb(config)
                }
            })
        })
    })
}

export function setConfigFlat(flat: FlatConfig) {
    chrome.storage.local.set({ "extension-config": flat })
}

export async function getConfigAndListen(cb: (n: IConfig) => any): Promise<IConfig> {
    let flat = await getConfigAndListenFlat(f => {
            cb(configFromFlat(f))
    })
    return configFromFlat(flat)
}