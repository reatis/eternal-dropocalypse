var e;
e || (e = typeof Module !== 'undefined' ? Module : {});
e = function(a) {
    function b(d) {
        if (c[d])
            return c[d].exports;
        var f = c[d] = {
            sc: d,
            Db: !1,
            exports: {}
        };
        a[d].call(f.exports, f, f.exports, b);
        f.Db = !0;
        return f.exports
    }
    var c = {};
    b.vc = a;
    b.oc = c;
    b.d = function(a, c, h) {
        b.Fb(a, c) || Object.defineProperty(a, c, {
            configurable: !1,
            enumerable: !0,
            get: h
        })
    }
    ;
    b.r = function(a) {
        Object.defineProperty(a, "__esModule", {
            value: !0
        })
    }
    ;
    b.n = function(a) {
        var c = a && a.kc ? function() {
            return a["default"]
        }
        : function() {
            return a
        }
        ;
        b.d(c, "a", c);
        return c
    }
    ;
    b.Fb = function(a, b) {
        return Object.prototype.hasOwnProperty.call(a, b)
    }
    ;
    b.p = "";
    return b(b.zc = 15)
}([function(a) {
    var b = function() {
        return this
    }();
    try {
        b = b || Function("return this")() || (0,
        eval)("this")
    } catch (c) {
        "object" === typeof window && (b = window)
    }
    a.exports = b
}
, function(a) {
    (function(b) {
        function c() {}
        function d(a, b) {
            return function() {
                a.apply(b, arguments)
            }
        }
        function f(a) {
            if ("object" !== typeof this)
                throw new TypeError("Promises must be constructed via new");
            if ("function" !== typeof a)
                throw new TypeError("not a function");
            this.i = 0;
            this.oa = !1;
            this.C = void 0;
            this.O = [];
            p(a, this)
        }
        function h(a, b) {
            for (; 3 === a.i; )
                a = a.C;
            0 === a.i ? a.O.push(b) : (a.oa = !0,
            f.aa(function() {
                var c = 1 === a.i ? b.Hb : b.Ib;
                if (null === c)
                    (1 === a.i ? g : k)(b.ha, a.C);
                else {
                    try {
                        var d = c(a.C)
                    } catch (G) {
                        k(b.ha, G);
                        return
                    }
                    g(b.ha, d)
                }
            }))
        }
        function g(a, b) {
            try {
                if (b === a)
                    throw new TypeError("A promise cannot be resolved with itself.");
                if (b && ("object" === typeof b || "function" === typeof b)) {
                    var c = b.then;
                    if (b instanceof f) {
                        a.i = 3;
                        a.C = b;
                        l(a);
                        return
                    }
                    if ("function" === typeof c) {
                        p(d(c, b), a);
                        return
                    }
                }
                a.i = 1;
                a.C = b;
                l(a)
            } catch (u) {
                k(a, u)
            }
        }
        function k(a, b) {
            a.i = 2;
            a.C = b;
            l(a)
        }
        function l(a) {
            2 === a.i && 0 === a.O.length && f.aa(function() {
                a.oa || f.ra(a.C)
            });
            for (var b = 0, c = a.O.length; b < c; b++)
                h(a, a.O[b]);
            a.O = null
        }
        function m(a, b, c) {
            this.Hb = "function" === typeof a ? a : null;
            this.Ib = "function" === typeof b ? b : null;
            this.ha = c
        }
        function p(a, b) {
            var c = !1;
            try {
                a(function(a) {
                    c || (c = !0,
                    g(b, a))
                }, function(a) {
                    c || (c = !0,
                    k(b, a))
                })
            } catch (u) {
                c || (c = !0,
                k(b, u))
            }
        }
        var t = setTimeout;
        f.prototype["catch"] = function(a) {
            return this.then(null, a)
        }
        ;
        f.prototype.then = function(a, b) {
            var d = new this.constructor(c);
            h(this, new m(a,b,d));
            return d
        }
        ;
        f.all = function(a) {
            var b = Array.prototype.slice.call(a);
            return new f(function(a, c) {
                function d(f, h) {
                    try {
                        if (h && ("object" === typeof h || "function" === typeof h)) {
                            var k = h.then;
                            if ("function" === typeof k) {
                                k.call(h, function(a) {
                                    d(f, a)
                                }, c);
                                return
                            }
                        }
                        b[f] = h;
                        0 === --r && a(b)
                    } catch (Ya) {
                        c(Ya)
                    }
                }
                if (0 === b.length)
                    return a([]);
                for (var r = b.length, f = 0; f < b.length; f++)
                    d(f, b[f])
            }
            )
        }
        ;
        f.resolve = function(a) {
            return a && "object" === typeof a && a.constructor === f ? a : new f(function(b) {
                b(a)
            }
            )
        }
        ;
        f.reject = function(a) {
            return new f(function(b, c) {
                c(a)
            }
            )
        }
        ;
        f.race = function(a) {
            return new f(function(b, c) {
                for (var d = 0, r = a.length; d < r; d++)
                    a[d].then(b, c)
            }
            )
        }
        ;
        f.aa = "function" === typeof setImmediate && function(a) {
            setImmediate(a)
        }
        || function(a) {
            t(a, 0)
        }
        ;
        f.ra = function(a) {
            "undefined" !== typeof console && console && console.warn("Possible Unhandled Promise Rejection:", a)
        }
        ;
        f.lc = function(a) {
            f.aa = a
        }
        ;
        f.mc = function(a) {
            f.ra = a
        }
        ;
        "undefined" !== typeof a && a.exports ? a.exports = f : b.Promise || (b.Promise = f)
    }
    )(this)
}
, function(a) {
    a.exports = e
}
, function(a, b, c) {
    var d = c(2);
    b = c(12).Ob;
    var f = c(9).Nb
      , h = c(8).ub;
    a.exports = d;
    d.sendFetchRequest = b;
    d.scheduleTask = f;
    d.WASM_BINARY_URL && (d.instantiateWasm = function(a, b) {
        return h(a, d.WASM_BINARY_URL, d.WASM_CACHE_MODE).then(b)
    }
    )
}
, function(a) {
    a.exports = {
        Pa: "player_loaded_in_worker",
        Ga: "master_manifest_request",
        Fa: "master_manifest_ready",
        Ia: "variant_request",
        Ha: "variant_ready",
        dc: "min_buffer_ready",
        ic: "video_element_play"
    }
}
, function(a) {
    a.exports = {
        Ja: "MetaID3",
        za: "MetaCaption",
        ab: "MetaSpliceOut",
        $a: "MetaSpliceIn"
    }
}
, function(a) {
    a.exports = {
        cc: "PlayerInitialized",
        Qa: "PlayerQualityChanged",
        ac: "PlayerAutoSwitchQualityChanged",
        Ba: "PlayerDurationChanged",
        jc: "PlayerVolumeChanged",
        ec: "PlayerMutedChanged",
        Oa: "PlayerPlaybackRateChanged",
        Ra: "PlayerRebuffering",
        $b: "PlayerAudioBlocked",
        fc: "PlayerPlaybackBlocked",
        Ea: "PlayerError",
        eb: "PlayerTracking",
        hc: "PlayerTimeUpdate",
        bc: "PlayerBufferUpdate",
        Wa: "PlayerSeekCompleted",
        M: "PlayerProfile",
        fb: "PlayerTwitchInfo",
        gb: "PlayerWorkerError",
        Ka: "PlayerMetadataCueEnter",
        La: "PlayerMetadataCueExit"
    }
}
, function(a) {
    a.exports = {
        bb: "ClientStateChanged",
        cb: "ClientStats",
        Va: "ClientSaveItem",
        Aa: "ClientConfigure",
        Sa: "ClientReinit",
        Da: "ClientEnqueue",
        Ca: "ClientEndOfStream",
        Za: "ClientSetTimestampOffset",
        Na: "ClientPlay",
        Ma: "ClientPause",
        Ta: "ClientRemove",
        Ua: "ClientReset",
        Xa: "ClientSeekTo",
        Ya: "ClientSetPlaybackRate",
        ya: "ClientAddCue"
    }
}
, function(a, b, c) {
    (function(a) {
        function d(a, b) {
            this.hb = a;
            this.pa = b
        }
        function h(a) {
            return new k(function(b, c) {
                var d = new XMLHttpRequest;
                d.open("GET", a);
                d.responseType = "arraybuffer";
                d.onreadystatechange = function() {
                    4 === d.readyState && (200 === d.status ? b(new Uint8Array(d.response)) : c())
                }
                ;
                d.onerror = c;
                d.send(null)
            }
            )
        }
        function g(a, b) {
            return k.resolve().then(function() {
                return WebAssembly.instantiateStreaming(fetch(b), a)
            }).catch(function() {
                return h(b).then(function(b) {
                    return WebAssembly.instantiate(b, a)
                })
            })
        }
        var k = a.Promise || c(1);
        b.ub = function(a, b, c, h) {
            h = h || "cvp-wasm-key";
            return d.open().then(function(d) {
                return d.get(h).then(function(r) {
                    return c ? r && b === r.url ? WebAssembly.instantiate(r.module, a) : g(a, b).then(function(a) {
                        d.put(h, {
                            url: b,
                            module: a.module
                        });
                        return a.instance
                    }) : k.reject()
                }).catch(function() {
                    d.clear();
                    return k.reject()
                })
            }).catch(function() {
                return g(a, b).then(function(a) {
                    return a.instance
                })
            })
        }
        ;
        d.open = function(a, b) {
            a = a || "cvp-wasm-db";
            b = b || "cvp-wasm-store";
            return new k(function(c, h) {
                var f = indexedDB.open(a, 1);
                f.onerror = h;
                f.onsuccess = function() {
                    c(new d(f.result,b))
                }
                ;
                f.onupgradeneeded = function() {
                    f.result.createObjectStore(b)
                }
            }
            )
        }
        ;
        d.prototype.get = function(a) {
            var b = this.da("readonly").get(a);
            return new k(function(a, c) {
                b.onsuccess = function() {
                    a(b.result)
                }
                ;
                b.onerror = c
            }
            )
        }
        ;
        d.prototype.put = function(a, b) {
            try {
                this.da("readwrite").put(b, a)
            } catch (p) {}
        }
        ;
        d.prototype.clear = function() {
            this.da("readwrite").clear()
        }
        ;
        d.prototype.da = function(a) {
            return this.hb.transaction(this.pa, a).objectStore(this.pa)
        }
    }
    ).call(this, c(0))
}
, function(a, b) {
    b.Nb = function(a, b, f) {
        var c = f ? setInterval(function() {
            a.call()
        }, b) : setTimeout(function() {
            a.call();
            a && (a["delete"](),
            a = null)
        }, b);
        a.setCancel(function() {
            a && (clearTimeout(c),
            a["delete"](),
            a = null)
        })
    }
}
, function(a) {
    a = a.exports = function() {
        this.N = [];
        this.J = this.G = 0
    }
    ;
    a.prototype.push = function(a) {
        this.J === this.N.length ? this.N.push(a) : this.N[this.J] = a;
        this.J++
    }
    ;
    a.prototype.pop = function() {
        var a = this.N[this.G];
        this.N[this.G] = null;
        this.G++;
        this.empty() && (this.J = this.G = 0);
        return a
    }
    ;
    a.prototype.size = function() {
        return this.J - this.G
    }
    ;
    a.prototype.empty = function() {
        return this.G >= this.J
    }
}
, function(a, b, c) {
    (function(b) {
        function d(a, b, c) {
            return new t(function(d, r) {
                var f = new XMLHttpRequest;
                f.open(c.method || "GET", b);
                for (var g in c.headers)
                    c.headers.hasOwnProperty(g) && f.setRequestHeader(g, c.headers[g]);
                var p = new h(f,a);
                f.addEventListener("readystatechange", function Ya() {
                    2 === f.readyState && (f.removeEventListener("readystatechange", Ya),
                    d(new k(f,p)))
                });
                c.Ub && (c.Ub.onabort = function() {
                    f.abort();
                    var a = Error("request aborted");
                    a.name = "AbortError";
                    p.B.na(a);
                    r(a)
                }
                );
                f.addEventListener("error", function() {
                    var a = Error("network error");
                    p.B.na(a);
                    r(a)
                });
                f.send(c.body || null)
            }
            )
        }
        function h(a, c) {
            var d = this.B = new g(a.abort.bind(a));
            a.responseType = c;
            switch (c) {
            case "moz-chunked-arraybuffer":
                a.addEventListener("progress", function() {
                    d._write(new Uint8Array(a.response))
                });
                a.addEventListener("load", d.S.bind(d));
                break;
            case "ms-stream":
                a.addEventListener("readystatechange", function() {
                    if (a.readyState == a.LOADING) {
                        var c = new b.MSStreamReader
                          , r = 0;
                        c.onprogress = function() {
                            c.result.byteLength > r && (d._write(new Uint8Array(c.result,r)),
                            r = c.result.byteLength)
                        }
                        ;
                        c.onload = d.S.bind(d);
                        c.readAsArrayBuffer(a.response)
                    }
                });
                break;
            case "arraybuffer":
                a.addEventListener("progress", d._write.bind(d, new Uint8Array(0))),
                a.addEventListener("load", function() {
                    a.response && d._write(new Uint8Array(a.response));
                    d.S()
                })
            }
        }
        function g(a) {
            this.i = "readable";
            this.H = new p;
            this.qa = this.o = null;
            this.jb = a
        }
        function k(a, b) {
            this.body = b;
            this.status = a.status;
            this.headers = new l(a)
        }
        function l(a) {
            this.mb = a
        }
        function m(a) {
            try {
                var b = new XMLHttpRequest;
                b.open("GET", "https://twitch.tv");
                b.responseType = a;
                return b.responseType === a ? a : ""
            } catch (u) {
                return ""
            }
        }
        var p = c(10)
          , t = b.Promise || c(1);
        h.prototype.getReader = function() {
            return this.B
        }
        ;
        var x = {
            done: !0,
            value: void 0
        };
        g.prototype.read = function() {
            switch (this.i) {
            case "readable":
                return this.H.empty() ? new t(function(a, b) {
                    this.o = {
                        resolve: a,
                        reject: b
                    }
                }
                .bind(this)) : this.H.pop();
            case "closed":
                return this.H.empty() ? t.resolve(x) : this.H.pop();
            case "errored":
                return t.reject(this.qa)
            }
        }
        ;
        g.prototype.cancel = function() {
            this.jb();
            this.S()
        }
        ;
        g.prototype._write = function(a) {
            switch (this.i) {
            case "readable":
                a = {
                    done: !1,
                    value: a
                },
                this.o ? (this.o.resolve(a),
                this.o = null) : this.H.push(t.resolve(a))
            }
        }
        ;
        g.prototype.S = function() {
            switch (this.i) {
            case "readable":
                this.o && (this.o.resolve(x),
                this.o = null),
                this.i = "closed"
            }
        }
        ;
        g.prototype.na = function(a) {
            switch (this.i) {
            case "readable":
                this.i = "errored",
                this.qa = a,
                this.H = null,
                this.o && (this.o.reject(a),
                this.o = null)
            }
        }
        ;
        l.prototype.get = function(a) {
            return this.mb.getResponseHeader(a)
        }
        ;
        a.exports = b.fetch && b.ReadableStream ? b.fetch : d.bind(null, m("moz-chunked-arraybuffer") || m("ms-stream") || "arraybuffer")
    }
    ).call(this, c(0))
}
, function(a, b, c) {
    function d(a) {
        this.ba = this.V = !1;
        this.B = this.I = null;
        this.ma = a;
        this.readBody = this.lb.bind(this)
    }
    var f = c(2)
      , h = c(11);
    b.Ob = function(a, b, c, f) {
        var k = null;
        //b = "data:application/octet-stream,"
        "undefined" !== typeof AbortController && (k = new AbortController,
        c.signal = k.signal);
        var g = new d(k)
          , r = setTimeout(function() {
            g.abort()
        }, f);
        h(b, c).then(function(b) {
            clearTimeout(r);
            g.Pb(b);
            g.V || a.response(g)
        }).catch(function(b) {
            clearTimeout(r);
            g.V || (console.error("HTTP Response Error:", b.name, b),
            a.error("AbortError" === b.name))
        }).then(function() {
            a["delete"]()
        });
        return function() {
            g.cancel()
        }
    }
    ;
    d.prototype.Pb = function(a) {
        this.I = a;
        this.ba && (this.ba = !1,
        this.T().cancel())
    }
    ;
    d.prototype.abort = function() {
        this.I ? this.T().cancel() : this.ma ? this.ma.abort() : this.ba = !0
    }
    ;
    d.prototype.cancel = function() {
        this.V = !0;
        this.abort()
    }
    ;
    d.prototype.getHeader = function(a) {
        return this.I.headers.get(a) || ""
    }
    ;
    d.prototype.getStatus = function() {
        return this.I.status
    }
    ;
    d.prototype.T = function() {
        this.B || (this.B = this.I.body.getReader());
        return this.B
    }
    ;
    d.prototype.lb = function(a, b) {
        var c = performance.now()
          , d = function() {
            var h = performance.now() - c;
            h < b ? f = setTimeout(d, b - h) : (this.abort(),
            a.error(!0))
        }
        .bind(this)
          , f = setTimeout(d, b)
          , h = function(b) {
            if (!this.V)
                if (b.done)
                    a.end();
                else {
                    var d = b.value.byteLength;
                    d && a.read(g(b.value), d);
                    c = performance.now();
                    return this.T().read().then(h)
                }
        }
        .bind(this);
        this.T().read().then(h).catch(function(b) {
            console.error("HTTP Read Error:", b.name, b, this.I);
            a.error(!1)
        }).then(function() {
            clearTimeout(f);
            a["delete"]()
        })
    }
    ;
    var g = function() {
        var a = 0
          , b = 0;
        return function(c) {
            var d = c.byteLength;
            d > b && (a && f._free(a),
            a = f._malloc(d),
            b = d);
            f.HEAPU8.set(c, a);
            return a
        }
    }()
}
, function(a, b, c) {
    (function(a) {
        function b(a) {
            function b(b) {
                if (k.calledRun) {
                    var r = b.data;
                    b = r.id;
                    var h = r.funcName;
                    r = r.args;
                    try {
                        "create" === h ? c[b] = new d(a,b,r[0]) : (c[b].Gb(h, r),
                        "delete" === h && delete c[b])
                    } catch (Bb) {
                        console.warn(Bb),
                        a.postMessage({
                            id: b,
                            type: m.gb,
                            arg: {
                                message: Bb.message
                            }
                        }, void 0)
                    }
                } else
                    f.push(b)
            }
            var c = Object.create(null)
              , f = [];
            if (!k.calledRun) {
                var h = k.onRuntimeInitialized;
                k.onRuntimeInitialized = function() {
                    h && h();
                    f.forEach(b);
                    f = null
                }
            }
            return b
        }
        function d(a, b, c) {
            this.ib = b;
            this.kb = a;
            this.ca = new k.WebMediaPlayer(this,c,c.browserContext);
            this.b(m.M, t.Pa)
        }
        function g(a) {
            try {
                return JSON.parse(a)
            } catch (r) {
                return console.error("Failed JSON parse:", a),
                {}
            }
        }
        var k = c(3)
          , l = c(7)
          , m = c(6)
          , p = c(5)
          , t = c(4);
        a.onmessage = b(a);
        a.onconnect = function(a) {
            a = a.ports[0];
            a.onmessage = b(a);
            a.start()
        }
        ;
        d.prototype.Gb = function(a, b) {
            this.ca[a].apply(this.ca, b)
        }
        ;
        d.prototype.b = function(a, b, c) {
            this.kb.postMessage({
                id: this.ib,
                type: a,
                arg: b
            }, c)
        }
        ;
        d.prototype.sendStats = function() {
            this.b(l.cb, this.ca.getStats())
        }
        ;
        d.prototype.sendTwitchInfo = function(a) {
            this.b(m.fb, {
                twitchInfo: a
            })
        }
        ;
        d.prototype.saveItem = function(a, b) {
            this.b(l.Va, {
                key: a,
                value: b
            })
        }
        ;
        d.prototype.onStateChanged = function(a) {
            this.b(l.bb, a)
        }
        ;
        d.prototype.onRebuffering = function() {
            this.b(m.Ra)
        }
        ;
        d.prototype.onQualityChanged = function(a) {
            this.b(m.Qa, {
                quality: a
            })
        }
        ;
        d.prototype.onSeekCompleted = function() {
            this.b(m.Wa)
        }
        ;
        d.prototype.onDurationChanged = function(a) {
            this.b(m.Ba, {
                duration: a
            })
        }
        ;
        d.prototype.onMetadata = function(a) {
            a = g(a);
            if ("ID3"in a) {
                var b = p.Ja;
                var c = a.ID3
            } else
                "caption"in a ? (b = p.za,
                c = a.caption) : "splice_out"in a ? (b = p.ab,
                c = a.splice_out) : "splice_in"in a ? (b = p.$a,
                c = a.splice_in) : "meta_enter"in a ? (b = m.Ka,
                c = a.meta_enter) : "meta_exit"in a && (b = m.La,
                c = a.meta_exit);
            this.b(b, c)
        }
        ;
        d.prototype.onError = function(a, b, c, d) {
            this.b(m.Ea, {
                type: a,
                code: b,
                source: c,
                message: d
            })
        }
        ;
        d.prototype.onAnalyticsEvent = function(a, b) {
            this.b(m.eb, {
                name: a,
                properties: g(b)
            })
        }
        ;
        d.prototype.configure = function(a, b, c, d, f) {
            this.b(l.Aa, {
                trackID: a,
                codec: b,
                path: c,
                isPassthrough: d,
                isProtected: f
            })
        }
        ;
        d.prototype.reinit = function() {
            this.b(l.Sa)
        }
        ;
        d.prototype.enqueue = function(a, b) {
            b = (new Uint8Array(b)).buffer;
            this.b(l.Da, {
                trackID: a,
                buffer: b
            }, [b])
        }
        ;
        d.prototype.endOfStream = function() {
            this.b(l.Ca)
        }
        ;
        d.prototype.setTimestampOffset = function(a, b) {
            this.b(l.Za, {
                trackID: a,
                offset: b
            })
        }
        ;
        d.prototype.play = function() {
            this.b(l.Na)
        }
        ;
        d.prototype.pause = function() {
            this.b(l.Ma)
        }
        ;
        d.prototype.reset = function() {
            this.b(l.Ua)
        }
        ;
        d.prototype.remove = function(a, b) {
            this.b(l.Ta, {
                start: a,
                end: b
            })
        }
        ;
        d.prototype.seekTo = function(a) {
            this.b(l.Xa, a)
        }
        ;
        d.prototype.setPlaybackRate = function(a) {
            this.b(l.Ya, a);
            this.b(m.Oa)
        }
        ;
        d.prototype.addCue = function(a, b, c) {
            this.b(l.ya, {
                id: a,
                start: b,
                end: c
            })
        }
        ;
        d.prototype.onMasterPlaylistRequest = function() {
            this.b(m.M, t.Ga)
        }
        ;
        d.prototype.onMasterPlaylistReady = function() {
            this.b(m.M, t.Fa)
        }
        ;
        d.prototype.onMediaPlaylistRequest = function() {
            this.b(m.M, t.Ia)
        }
        ;
        d.prototype.onMediaPlaylistReady = function() {
            this.b(m.M, t.Ha)
        }
    }
    ).call(this, c(0))
}
, function(a, b, c) {
    (function(a) {
        (function() {
            if (!("performance"in a && "now"in a.performance)) {
                var b = Date.now();
                a.performance = {
                    now: function() {
                        return Date.now() - b
                    }
                }
            }
        }
        )()
    }
    ).call(this, c(0))
}
, function(a, b, c) {
    c(14);
    c(13);
    a.exports = c(3)
}
]);
var aa = {}, n;
for (n in e)
    e.hasOwnProperty(n) && (aa[n] = e[n]);
e.arguments = [];
e.thisProgram = "./this.program";
e.quit = function(a, b) {
    throw b;
}
;
e.preRun = [];
e.postRun = [];
var w = !1
  , y = !1
  , ba = !1;
if (e.ENVIRONMENT)
    if ("WEB" === e.ENVIRONMENT)
        w = !0;
    else if ("WORKER" === e.ENVIRONMENT)
        y = !0;
    else if ("NODE" === e.ENVIRONMENT)
        ba = !0;
    else {
        if ("SHELL" !== e.ENVIRONMENT)
            throw Error("Module['ENVIRONMENT'] value is not valid. must be one of: WEB|WORKER|NODE|SHELL.");
    }
else
    w = "object" === typeof window,
    y = "function" === typeof importScripts,
    ba = "object" === typeof process && "function" === typeof require && !w && !y;
if (w || y)
    e.read = function(a) {
        var b = new XMLHttpRequest;
        b.open("GET", a, !1);
        b.send(null);
        return b.responseText
    }
    ,
    y && (e.readBinary = function(a) {
        var b = new XMLHttpRequest;
        b.open("GET", a, !1);
        b.responseType = "arraybuffer";
        b.send(null);
        return new Uint8Array(b.response)
    }
    ),
    e.readAsync = function(a, b, c) {
        var d = new XMLHttpRequest;
        d.open("GET", a, !0);
        d.responseType = "arraybuffer";
        d.onload = function() {
            200 == d.status || 0 == d.status && d.response ? b(d.response) : c()
        }
        ;
        d.onerror = c;
        d.send(null)
    }
    ,
    e.setWindowTitle = function(a) {
        document.title = a
    }
    ;
else
    throw Error("not compiled for this environment");
e.print = "undefined" !== typeof console ? console.log.bind(console) : "undefined" !== typeof print ? print : null;
e.printErr = "undefined" !== typeof printErr ? printErr : "undefined" !== typeof console && console.warn.bind(console) || e.print;
e.print = e.print;
e.printErr = e.printErr;
for (n in aa)
    aa.hasOwnProperty(n) && (e[n] = aa[n]);
aa = void 0;
function ca(a) {
    assert(!da);
    var b = z;
    z = z + a + 15 & -16;
    return b
}
function ea(a) {
    assert(A);
    var b = B[A >> 2];
    a = b + a + 15 & -16;
    B[A >> 2] = a;
    return a >= C && !fa() ? (B[A >> 2] = b,
    0) : b
}
function ha(a) {
    var b;
    b || (b = 16);
    return Math.ceil(a / b) * b
}
var ka;
function la(a, b, c) {
    return c ? +(a >>> 0) + 4294967296 * +(b >>> 0) : +(a >>> 0) + 4294967296 * +(b | 0)
}
var ma = 0;
function assert(a, b) {
    a || D("Assertion failed: " + b)
}
function na(a, b) {
    if ("number" === typeof a) {
        var c = !0;
        var d = a
    } else
        c = !1,
        d = a.length;
    b = 4 == b ? f : ["function" === typeof E ? E : ca, oa, ca, ea][void 0 === b ? 2 : b](Math.max(d, 1));
    if (c) {
        var f = b;
        assert(0 == (b & 3));
        for (a = b + (d & -4); f < a; f += 4)
            B[f >> 2] = 0;
        for (a = b + d; f < a; )
            F[f++ >> 0] = 0;
        return b
    }
    a.subarray || a.slice ? H.set(a, b) : H.set(new Uint8Array(a), b);
    return b
}
function pa(a) {
    var b;
    if (0 === b || !a)
        return "";
    for (var c = 0, d, f = 0; ; ) {
        d = H[a + f >> 0];
        c |= d;
        if (0 == d && !b)
            break;
        f++;
        if (b && f == b)
            break
    }
    b || (b = f);
    d = "";
    if (128 > c) {
        for (; 0 < b; )
            c = String.fromCharCode.apply(String, H.subarray(a, a + Math.min(b, 1024))),
            d = d ? d + c : c,
            a += 1024,
            b -= 1024;
        return d
    }
    return qa(H, a)
}
var ra = "undefined" !== typeof TextDecoder ? new TextDecoder("utf8") : void 0;
function qa(a, b) {
    for (var c = b; a[c]; )
        ++c;
    if (16 < c - b && a.subarray && ra)
        return ra.decode(a.subarray(b, c));
    for (c = ""; ; ) {
        var d = a[b++];
        if (!d)
            return c;
        if (d & 128) {
            var f = a[b++] & 63;
            if (192 == (d & 224))
                c += String.fromCharCode((d & 31) << 6 | f);
            else {
                var h = a[b++] & 63;
                if (224 == (d & 240))
                    d = (d & 15) << 12 | f << 6 | h;
                else {
                    var g = a[b++] & 63;
                    if (240 == (d & 248))
                        d = (d & 7) << 18 | f << 12 | h << 6 | g;
                    else {
                        var k = a[b++] & 63;
                        if (248 == (d & 252))
                            d = (d & 3) << 24 | f << 18 | h << 12 | g << 6 | k;
                        else {
                            var l = a[b++] & 63;
                            d = (d & 1) << 30 | f << 24 | h << 18 | g << 12 | k << 6 | l
                        }
                    }
                }
                65536 > d ? c += String.fromCharCode(d) : (d -= 65536,
                c += String.fromCharCode(55296 | d >> 10, 56320 | d & 1023))
            }
        } else
            c += String.fromCharCode(d)
    }
}
function sa(a, b, c, d) {
    if (!(0 < d))
        return 0;
    var f = c;
    d = c + d - 1;
    for (var h = 0; h < a.length; ++h) {
        var g = a.charCodeAt(h);
        55296 <= g && 57343 >= g && (g = 65536 + ((g & 1023) << 10) | a.charCodeAt(++h) & 1023);
        if (127 >= g) {
            if (c >= d)
                break;
            b[c++] = g
        } else {
            if (2047 >= g) {
                if (c + 1 >= d)
                    break;
                b[c++] = 192 | g >> 6
            } else {
                if (65535 >= g) {
                    if (c + 2 >= d)
                        break;
                    b[c++] = 224 | g >> 12
                } else {
                    if (2097151 >= g) {
                        if (c + 3 >= d)
                            break;
                        b[c++] = 240 | g >> 18
                    } else {
                        if (67108863 >= g) {
                            if (c + 4 >= d)
                                break;
                            b[c++] = 248 | g >> 24
                        } else {
                            if (c + 5 >= d)
                                break;
                            b[c++] = 252 | g >> 30;
                            b[c++] = 128 | g >> 24 & 63
                        }
                        b[c++] = 128 | g >> 18 & 63
                    }
                    b[c++] = 128 | g >> 12 & 63
                }
                b[c++] = 128 | g >> 6 & 63
            }
            b[c++] = 128 | g & 63
        }
    }
    b[c] = 0;
    return c - f
}
function ta(a) {
    for (var b = 0, c = 0; c < a.length; ++c) {
        var d = a.charCodeAt(c);
        55296 <= d && 57343 >= d && (d = 65536 + ((d & 1023) << 10) | a.charCodeAt(++c) & 1023);
        127 >= d ? ++b : b = 2047 >= d ? b + 2 : 65535 >= d ? b + 3 : 2097151 >= d ? b + 4 : 67108863 >= d ? b + 5 : b + 6
    }
    return b
}
"undefined" !== typeof TextDecoder && new TextDecoder("utf-16le");
var ua = 65536
  , va = 16777216
  , wa = 16777216;
function xa(a, b) {
    0 < a % b && (a += b - a % b);
    return a
}
var buffer, F, H, ya, za, B, I, Aa, Ba;
function Ca() {
    e.HEAP8 = F = new Int8Array(buffer);
    e.HEAP16 = ya = new Int16Array(buffer);
    e.HEAP32 = B = new Int32Array(buffer);
    e.HEAPU8 = H = new Uint8Array(buffer);
    e.HEAPU16 = za = new Uint16Array(buffer);
    e.HEAPU32 = I = new Uint32Array(buffer);
    e.HEAPF32 = Aa = new Float32Array(buffer);
    e.HEAPF64 = Ba = new Float64Array(buffer)
}
var Da, z, da, Ea, Fa, Ga, Ha, A;
Da = z = Ea = Fa = Ga = Ha = A = 0;
da = !1;
e.reallocBuffer || (e.reallocBuffer = function(a) {
    try {
        if (ArrayBuffer.Zb)
            var b = ArrayBuffer.Zb(buffer, a);
        else {
            var c = F;
            b = new ArrayBuffer(a);
            (new Int8Array(b)).set(c)
        }
    } catch (d) {
        return !1
    }
    return Ia(b) ? b : !1
}
);
function fa() {
    var a = e.usingWasm ? ua : va
      , b = 2147483648 - a;
    if (B[A >> 2] > b)
        return !1;
    var c = C;
    for (C = Math.max(C, wa); C < B[A >> 2]; )
        C = 536870912 >= C ? xa(2 * C, a) : Math.min(xa((3 * C + 2147483648) / 4, a), b);
    a = e.reallocBuffer(C);
    if (!a || a.byteLength != C)
        return C = c,
        !1;
    e.buffer = buffer = a;
    Ca();
    return !0
}
var Ja;
try {
    Ja = Function.prototype.call.bind(Object.getOwnPropertyDescriptor(ArrayBuffer.prototype, "byteLength").get),
    Ja(new ArrayBuffer(4))
} catch (a) {
    Ja = function(b) {
        return b.byteLength
    }
}
var Ka = e.TOTAL_STACK || 5242880
  , C = e.TOTAL_MEMORY || 33554432;
C < Ka && e.printErr("TOTAL_MEMORY should be larger than TOTAL_STACK, was " + C + "! (TOTAL_STACK=" + Ka + ")");
e.buffer ? buffer = e.buffer : ("object" === typeof WebAssembly && "function" === typeof WebAssembly.Memory ? (e.wasmMemory = new WebAssembly.Memory({
    initial: C / ua
}),
buffer = e.wasmMemory.buffer) : buffer = new ArrayBuffer(C),
e.buffer = buffer);
Ca();
B[0] = 1668509029;
ya[1] = 25459;
if (115 !== H[2] || 99 !== H[3])
    throw "Runtime error: expected the system to be little-endian!";
function La(a) {
    for (; 0 < a.length; ) {
        var b = a.shift();
        if ("function" == typeof b)
            b();
        else {
            var c = b.f;
            "number" === typeof c ? void 0 === b.ea ? e.dynCall_v(c) : e.dynCall_vi(c, b.ea) : c(void 0 === b.ea ? null : b.ea)
        }
    }
}
var Ma = []
  , Na = []
  , Oa = []
  , Pa = []
  , Qa = []
  , Ra = !1;
function Sa() {
    var a = e.preRun.shift();
    Ma.unshift(a)
}
function Ta(a, b) {
    return 0 <= a ? a : 32 >= b ? 2 * Math.abs(1 << b - 1) + a : Math.pow(2, b) + a
}
function Ua(a, b) {
    if (0 >= a)
        return a;
    var c = 32 >= b ? Math.abs(1 << b - 1) : Math.pow(2, b - 1);
    a >= c && (32 >= b || a > c) && (a = -2 * c + a);
    return a
}
var J = 0
  , Va = null
  , Wa = null;
e.preloadedImages = {};
e.preloadedAudios = {};
function Xa(a) {
    return String.prototype.startsWith ? a.startsWith("data:application/octet-stream;base64,") : 0 === a.indexOf("data:application/octet-stream;base64,")
}
(function() {
    function a() {
        try {
            if (e.wasmBinary)
                return new Uint8Array(e.wasmBinary);
            if (e.readBinary)
                return e.readBinary(f);
            throw "on the web, we need the wasm binary to be preloaded and set on Module['wasmBinary']. emcc.py will do that for you when generating HTML (but not JS)";
        } catch (p) {
            D(p)
        }
    }
    function b() {
        return e.wasmBinary || !w && !y || "function" !== typeof fetch ? new Promise(function(b) {
            b(a())
        }
        ) : fetch(f, {
            credentials: "same-origin"
        }).then(function(a) {
            if (!a.ok)
                throw "failed to load wasm binary file at '" + f + "'";
            return a.arrayBuffer()
        }).catch(function() {
            return a()
        })
    }
    function c(a) {
        function c(a) {
            k = a.exports;
            if (k.memory) {
                a = k.memory;
                var b = e.buffer;
                a.byteLength < b.byteLength && e.printErr("the new buffer in mergeMemory is smaller than the previous one. in native wasm, we should grow memory here");
                b = new Int8Array(b);
                (new Int8Array(a)).set(b);
                e.buffer = buffer = a;
                Ca()
            }
            e.asm = k;
            e.usingWasm = !0;
            J--;
            e.monitorRunDependencies && e.monitorRunDependencies(J);
            0 == J && (null !== Va && (clearInterval(Va),
            Va = null),
            Wa && (a = Wa,
            Wa = null,
            a()))
        }
        function d(a) {
            c(a.instance)
        }
        function h(a) {
            b().then(function(a) {
                return WebAssembly.instantiate(a, g)
            }).then(a).catch(function(a) {
                e.printErr("failed to asynchronously prepare wasm: " + a);
                D(a)
            })
        }
        if ("object" !== typeof WebAssembly)
            return e.printErr("no native wasm support detected"),
            !1;
        if (!(e.wasmMemory instanceof WebAssembly.Memory))
            return e.printErr("no native wasm Memory in use"),
            !1;
        a.memory = e.wasmMemory;
        g.global = {
            NaN: NaN,
            Infinity: Infinity
        };
        g["global.Math"] = Math;
        g.env = a;
        J++;
        e.monitorRunDependencies && e.monitorRunDependencies(J);
        if (e.instantiateWasm)
            try {
                return e.instantiateWasm(g, c)
            } catch (v) {
                return e.printErr("Module.instantiateWasm callback failed with error: " + v),
                !1
            }
        e.wasmBinary || "function" !== typeof WebAssembly.instantiateStreaming || Xa(f) || "function" !== typeof fetch ? h(d) : WebAssembly.instantiateStreaming(fetch(f, {
            credentials: "same-origin"
        }), g).then(d).catch(function(a) {
            e.printErr("wasm streaming compile failed: " + a);
            e.printErr("falling back to ArrayBuffer instantiation");
            h(d)
        });
        return {}
    }
    var d = "wasmworker.min.wast"
      , f = "wasmworker.min.wasm"
      , h = "wasmworker.min.temp.asm.js";
    "function" === typeof e.locateFile && (Xa(d) || (d = e.locateFile(d)),
    Xa(f) || (f = e.locateFile(f)),
    Xa(h) || (h = e.locateFile(h)));
    var g = {
        global: null,
        env: null,
        asm2wasm: {
            "f64-rem": function(a, b) {
                return a % b
            },
            "debugger": function() {
                debugger
            },
            "f64-to-int": function(a) {
                return a | 0
            },
            "i32s-div": function(a, b) {
                return (a | 0) / (b | 0) | 0
            },
            "i32u-div": function(a, b) {
                return (a >>> 0) / (b >>> 0) >>> 0
            },
            "i32s-rem": function(a, b) {
                return (a | 0) % (b | 0) | 0
            },
            "i32u-rem": function(a, b) {
                return (a >>> 0) % (b >>> 0) >>> 0
            }
        },
        parent: e
    }
      , k = null;
    e.asmPreload = e.asm;
    var l = e.reallocBuffer;
    e.reallocBuffer = function(a) {
        if ("asmjs" === m)
            var b = l(a);
        else
            a: {
                a = xa(a, e.usingWasm ? ua : va);
                var c = e.buffer.byteLength;
                if (e.usingWasm)
                    try {
                        b = -1 !== e.wasmMemory.grow((a - c) / 65536) ? e.buffer = e.wasmMemory.buffer : null;
                        break a
                    } catch (r) {
                        b = null;
                        break a
                    }
                b = void 0
            }
        return b
    }
    ;
    var m = "";
    e.asm = function(a, b) {
        if (!b.table) {
            a = e.wasmTableSize;
            void 0 === a && (a = 1024);
            var d = e.wasmMaxTableSize;
            b.table = "object" === typeof WebAssembly && "function" === typeof WebAssembly.Table ? void 0 !== d ? new WebAssembly.Table({
                initial: a,
                maximum: d,
                element: "anyfunc"
            }) : new WebAssembly.Table({
                initial: a,
                element: "anyfunc"
            }) : Array(a);
            e.wasmTable = b.table
        }
        b.memoryBase || (b.memoryBase = e.STATIC_BASE);
        b.tableBase || (b.tableBase = 0);
        b = c(b);
        assert(b, "no binaryen method succeeded.");
        return b
    }
}
)();
Da = 1024;
z = Da + 80416;
Na.push({
    f: function() {
        Za()
    }
}, {
    f: function() {
        $a()
    }
}, {
    f: function() {
        ab()
    }
}, {
    f: function() {
        bb()
    }
}, {
    f: function() {
        cb()
    }
}, {
    f: function() {
        db()
    }
}, {
    f: function() {
        eb()
    }
}, {
    f: function() {
        fb()
    }
}, {
    f: function() {
        gb()
    }
}, {
    f: function() {
        hb()
    }
}, {
    f: function() {
        ib()
    }
}, {
    f: function() {
        jb()
    }
}, {
    f: function() {
        kb()
    }
}, {
    f: function() {
        lb()
    }
}, {
    f: function() {
        mb()
    }
}, {
    f: function() {
        nb()
    }
}, {
    f: function() {
        ob()
    }
}, {
    f: function() {
        pb()
    }
}, {
    f: function() {
        qb()
    }
}, {
    f: function() {
        rb()
    }
}, {
    f: function() {
        sb()
    }
}, {
    f: function() {
        tb()
    }
}, {
    f: function() {
        ub()
    }
}, {
    f: function() {
        vb()
    }
}, {
    f: function() {
        wb()
    }
});
e.STATIC_BASE = Da;
e.STATIC_BUMP = 80416;
z += 16;
function xb() {
    return !!xb.xa
}
function yb(a) {
    e.___errno_location && (B[e.___errno_location() >> 2] = a);
    return a
}
var zb = 0;
function K() {
    zb += 4;
    return B[zb - 4 >> 2]
}
var Ab = {};
function Cb(a, b) {
    zb = b;
    try {
        var c = K()
          , d = K()
          , f = K();
        a = 0;
        Cb.sa || (Cb.sa = [null, [], []],
        Cb.Kb = function(a, b) {
            var c = Cb.sa[a];
            assert(c);
            0 === b || 10 === b ? ((1 === a ? e.print : e.printErr)(qa(c, 0)),
            c.length = 0) : c.push(b)
        }
        );
        for (b = 0; b < f; b++) {
            for (var h = B[d + 8 * b >> 2], g = B[d + (8 * b + 4) >> 2], k = 0; k < g; k++)
                Cb.Kb(c, H[h + k]);
            a += g
        }
        return a
    } catch (l) {
        return "undefined" !== typeof FS && l instanceof FS.$ || D(l),
        -l.fa
    }
}
var Db = {};
function Eb(a) {
    for (; a.length; ) {
        var b = a.pop();
        a.pop()(b)
    }
}
function Fb(a) {
    return this.fromWireType(I[a >> 2])
}
var Gb = {}
  , L = {}
  , Hb = {};
function Ib(a) {
    if (void 0 === a)
        return "_unknown";
    a = a.replace(/[^a-zA-Z0-9_]/g, "$");
    var b = a.charCodeAt(0);
    return 48 <= b && 57 >= b ? "_" + a : a
}
function Jb(a, b) {
    a = Ib(a);
    return (new Function("body","return function " + a + '() {\n    "use strict";    return body.apply(this, arguments);\n};\n'))(b)
}
function Kb(a) {
    var b = Error
      , c = Jb(a, function(b) {
        this.name = a;
        this.message = b;
        b = Error(b).stack;
        void 0 !== b && (this.stack = this.toString() + "\n" + b.replace(/^Error(:[^\n]*)?\n/, ""))
    });
    c.prototype = Object.create(b.prototype);
    c.prototype.constructor = c;
    c.prototype.toString = function() {
        return void 0 === this.message ? this.name : this.name + ": " + this.message
    }
    ;
    return c
}
var Lb = void 0;
function Mb(a) {
    throw new Lb(a);
}
function M(a, b, c) {
    function d(b) {
        b = c(b);
        b.length !== a.length && Mb("Mismatched type converter count");
        for (var d = 0; d < a.length; ++d)
            N(a[d], b[d])
    }
    a.forEach(function(a) {
        Hb[a] = b
    });
    var f = Array(b.length)
      , h = []
      , g = 0;
    b.forEach(function(a, b) {
        L.hasOwnProperty(a) ? f[b] = L[a] : (h.push(a),
        Gb.hasOwnProperty(a) || (Gb[a] = []),
        Gb[a].push(function() {
            f[b] = L[a];
            ++g;
            g === h.length && d(f)
        }))
    });
    0 === h.length && d(f)
}
function Nb(a) {
    switch (a) {
    case 1:
        return 0;
    case 2:
        return 1;
    case 4:
        return 2;
    case 8:
        return 3;
    default:
        throw new TypeError("Unknown type size: " + a);
    }
}
var Ob = void 0;
function O(a) {
    for (var b = ""; H[a]; )
        b += Ob[H[a++]];
    return b
}
var Pb = void 0;
function P(a) {
    throw new Pb(a);
}
function N(a, b, c) {
    c = c || {};
    if (!("argPackAdvance"in b))
        throw new TypeError("registerType registeredInstance requires argPackAdvance");
    var d = b.name;
    a || P('type "' + d + '" must have a positive integer typeid pointer');
    if (L.hasOwnProperty(a)) {
        if (c.Bb)
            return;
        P("Cannot register type '" + d + "' twice")
    }
    L[a] = b;
    delete Hb[a];
    Gb.hasOwnProperty(a) && (b = Gb[a],
    delete Gb[a],
    b.forEach(function(a) {
        a()
    }))
}
function Qb(a) {
    P(a.a.h.c.name + " instance already deleted")
}
var Rb = void 0
  , Sb = [];
function Tb() {
    for (; Sb.length; ) {
        var a = Sb.pop();
        a.a.K = !1;
        a["delete"]()
    }
}
function Q() {}
var Ub = {};
function Vb(a, b, c) {
    if (void 0 === a[b].v) {
        var d = a[b];
        a[b] = function() {
            a[b].v.hasOwnProperty(arguments.length) || P("Function '" + c + "' called with an invalid number of arguments (" + arguments.length + ") - expects one of (" + a[b].v + ")!");
            return a[b].v[arguments.length].apply(this, arguments)
        }
        ;
        a[b].v = [];
        a[b].v[d.U] = d
    }
}
function Wb(a, b) {
    e.hasOwnProperty(a) ? (P("Cannot register public name '" + a + "' twice"),
    Vb(e, a, a),
    e.hasOwnProperty(void 0) && P("Cannot register multiple overloads of a function with the same number of arguments (undefined)!"),
    e[a].v[void 0] = b) : e[a] = b
}
function Xb(a, b, c, d, f, h, g, k) {
    this.name = a;
    this.constructor = b;
    this.L = c;
    this.w = d;
    this.s = f;
    this.tb = h;
    this.R = g;
    this.rb = k;
    this.Lb = []
}
function Yb(a, b, c) {
    for (; b !== c; )
        b.R || P("Expected null or instance of " + c.name + ", got an instance of " + b.name),
        a = b.R(a),
        b = b.s;
    return a
}
function Zb(a, b) {
    if (null === b)
        return this.ga && P("null is not a valid " + this.name),
        0;
    b.a || P('Cannot pass "' + $b(b) + '" as a ' + this.name);
    b.a.g || P("Cannot pass deleted object as a pointer of type " + this.name);
    return Yb(b.a.g, b.a.h.c, this.c)
}
function ac(a, b) {
    if (null === b) {
        this.ga && P("null is not a valid " + this.name);
        if (this.X) {
            var c = this.ia();
            null !== a && a.push(this.w, c);
            return c
        }
        return 0
    }
    b.a || P('Cannot pass "' + $b(b) + '" as a ' + this.name);
    b.a.g || P("Cannot pass deleted object as a pointer of type " + this.name);
    !this.W && b.a.h.W && P("Cannot convert argument of type " + (b.a.m ? b.a.m.name : b.a.h.name) + " to parameter type " + this.name);
    c = Yb(b.a.g, b.a.h.c, this.c);
    if (this.X)
        switch (void 0 === b.a.l && P("Passing raw pointer to smart pointer is illegal"),
        this.Tb) {
        case 0:
            b.a.m === this ? c = b.a.l : P("Cannot convert argument of type " + (b.a.m ? b.a.m.name : b.a.h.name) + " to parameter type " + this.name);
            break;
        case 1:
            c = b.a.l;
            break;
        case 2:
            if (b.a.m === this)
                c = b.a.l;
            else {
                var d = b.clone();
                c = this.Mb(c, R(function() {
                    d["delete"]()
                }));
                null !== a && a.push(this.w, c)
            }
            break;
        default:
            P("Unsupporting sharing policy")
        }
    return c
}
function bc(a, b) {
    if (null === b)
        return this.ga && P("null is not a valid " + this.name),
        0;
    b.a || P('Cannot pass "' + $b(b) + '" as a ' + this.name);
    b.a.g || P("Cannot pass deleted object as a pointer of type " + this.name);
    b.a.h.W && P("Cannot convert argument of type " + b.a.h.name + " to parameter type " + this.name);
    return Yb(b.a.g, b.a.h.c, this.c)
}
function cc(a, b, c) {
    if (b === c)
        return a;
    if (void 0 === c.s)
        return null;
    a = cc(a, b, c.s);
    return null === a ? null : c.rb(a)
}
var dc = {};
function ec(a, b) {
    for (void 0 === b && P("ptr should not be undefined"); a.s; )
        b = a.R(b),
        a = a.s;
    return dc[b]
}
function fc(a, b) {
    b.h && b.g || Mb("makeClassHandle requires ptr and ptrType");
    !!b.m !== !!b.l && Mb("Both smartPtrType and smartPtr must be specified");
    b.count = {
        value: 1
    };
    return Object.create(a, {
        a: {
            value: b
        }
    })
}
function S(a, b, c, d, f, h, g, k, l, m, p) {
    this.name = a;
    this.c = b;
    this.ga = c;
    this.W = d;
    this.X = f;
    this.Jb = h;
    this.Tb = g;
    this.va = k;
    this.ia = l;
    this.Mb = m;
    this.w = p;
    f || void 0 !== b.s ? this.toWireType = ac : (this.toWireType = d ? Zb : bc,
    this.u = null)
}
function hc(a, b) {
    e.hasOwnProperty(a) || Mb("Replacing nonexistant public symbol");
    e[a] = b;
    e[a].U = void 0
}
function T(a, b) {
    a = O(a);
    if (void 0 !== e["FUNCTION_TABLE_" + a])
        var c = e["FUNCTION_TABLE_" + a][b];
    else if ("undefined" !== typeof FUNCTION_TABLE)
        c = FUNCTION_TABLE[b];
    else {
        c = e.asm["dynCall_" + a];
        void 0 === c && (c = e.asm["dynCall_" + a.replace(/f/g, "d")],
        void 0 === c && P("No dynCall invoker for signature: " + a));
        for (var d = [], f = 1; f < a.length; ++f)
            d.push("a" + f);
        f = "return function " + ("dynCall_" + a + "_" + b) + "(" + d.join(", ") + ") {\n";
        f += "    return dynCall(rawFunction" + (d.length ? ", " : "") + d.join(", ") + ");\n";
        c = (new Function("dynCall","rawFunction",f + "};\n"))(c, b)
    }
    "function" !== typeof c && P("unknown function pointer with signature " + a + ": " + b);
    return c
}
var ic = void 0;
function kc(a) {
    a = lc(a);
    var b = O(a);
    U(a);
    return b
}
function mc(a, b) {
    function c(a) {
        f[a] || L[a] || (Hb[a] ? Hb[a].forEach(c) : (d.push(a),
        f[a] = !0))
    }
    var d = []
      , f = {};
    b.forEach(c);
    throw new ic(a + ": " + d.map(kc).join([", "]));
}
function nc(a, b) {
    for (var c = [], d = 0; d < a; d++)
        c.push(B[(b >> 2) + d]);
    return c
}
function oc(a) {
    var b = Function;
    if (!(b instanceof Function))
        throw new TypeError("new_ called with constructor type " + typeof b + " which is not a function");
    var c = Jb(b.name || "unknownFunctionName", function() {});
    c.prototype = b.prototype;
    c = new c;
    a = b.apply(c, a);
    return a instanceof Object ? a : c
}
var pc = []
  , V = [{}, {
    value: void 0
}, {
    value: null
}, {
    value: !0
}, {
    value: !1
}];
function qc(a) {
    4 < a && 0 === --V[a].ja && (V[a] = void 0,
    pc.push(a))
}
function R(a) {
    switch (a) {
    case void 0:
        return 1;
    case null:
        return 2;
    case !0:
        return 3;
    case !1:
        return 4;
    default:
        var b = pc.length ? pc.pop() : V.length;
        V[b] = {
            ja: 1,
            value: a
        };
        return b
    }
}
function $b(a) {
    if (null === a)
        return "null";
    var b = typeof a;
    return "object" === b || "array" === b || "function" === b ? a.toString() : "" + a
}
function rc(a, b) {
    switch (b) {
    case 2:
        return function(a) {
            return this.fromWireType(Aa[a >> 2])
        }
        ;
    case 3:
        return function(a) {
            return this.fromWireType(Ba[a >> 3])
        }
        ;
    default:
        throw new TypeError("Unknown float type: " + a);
    }
}
function sc(a, b, c) {
    switch (b) {
    case 0:
        return c ? function(a) {
            return F[a]
        }
        : function(a) {
            return H[a]
        }
        ;
    case 1:
        return c ? function(a) {
            return ya[a >> 1]
        }
        : function(a) {
            return za[a >> 1]
        }
        ;
    case 2:
        return c ? function(a) {
            return B[a >> 2]
        }
        : function(a) {
            return I[a >> 2]
        }
        ;
    default:
        throw new TypeError("Unknown integer type: " + a);
    }
}
function W(a) {
    a || P("Cannot use deleted val. handle = " + a);
    return V[a].value
}
function tc(a, b) {
    var c = L[a];
    void 0 === c && P(b + " has unknown type " + kc(a));
    return c
}
function uc(a, b) {
    for (var c = Array(a), d = 0; d < a; ++d)
        c[d] = tc(B[(b >> 2) + d], "parameter " + d);
    return c
}
var vc = {};
function wc(a) {
    var b = vc[a];
    return void 0 === b ? O(a) : b
}
var xc = [];
function yc(a) {
    var b = xc.length;
    xc.push(a);
    return b
}
function zc() {
    D()
}
function Ac(a, b) {
    function c(a) {
        var b = d;
        "double" === a || "i64" === a ? b & 7 && (assert(4 === (b & 7)),
        b += 4) : assert(0 === (b & 3));
        d = b;
        "double" === a ? (a = Ba[d >> 3],
        d += 8) : "i64" == a ? (a = [B[d >> 2], B[d + 4 >> 2]],
        d += 8) : (assert(0 === (d & 3)),
        a = B[d >> 2],
        d += 4);
        return a
    }
    assert(0 === (b & 3));
    for (var d = b, f = [], h, g; ; ) {
        var k = a;
        h = F[a >> 0];
        if (0 === h)
            break;
        g = F[a + 1 >> 0];
        if (37 == h) {
            var l = !1
              , m = b = !1
              , p = !1
              , t = !1;
            a: for (; ; ) {
                switch (g) {
                case 43:
                    l = !0;
                    break;
                case 45:
                    b = !0;
                    break;
                case 35:
                    m = !0;
                    break;
                case 48:
                    if (p)
                        break a;
                    else {
                        p = !0;
                        break
                    }
                case 32:
                    t = !0;
                    break;
                default:
                    break a
                }
                a++;
                g = F[a + 1 >> 0]
            }
            var x = 0;
            if (42 == g)
                x = c("i32"),
                a++,
                g = F[a + 1 >> 0];
            else
                for (; 48 <= g && 57 >= g; )
                    x = 10 * x + (g - 48),
                    a++,
                    g = F[a + 1 >> 0];
            var r = !1
              , v = -1;
            if (46 == g) {
                v = 0;
                r = !0;
                a++;
                g = F[a + 1 >> 0];
                if (42 == g)
                    v = c("i32"),
                    a++;
                else
                    for (; ; ) {
                        g = F[a + 1 >> 0];
                        if (48 > g || 57 < g)
                            break;
                        v = 10 * v + (g - 48);
                        a++
                    }
                g = F[a + 1 >> 0]
            }
            0 > v && (v = 6,
            r = !1);
            switch (String.fromCharCode(g)) {
            case "h":
                g = F[a + 2 >> 0];
                if (104 == g) {
                    a++;
                    var u = 1
                } else
                    u = 2;
                break;
            case "l":
                g = F[a + 2 >> 0];
                108 == g ? (a++,
                u = 8) : u = 4;
                break;
            case "L":
            case "q":
            case "j":
                u = 8;
                break;
            case "z":
            case "t":
            case "I":
                u = 4;
                break;
            default:
                u = null
            }
            u && a++;
            g = F[a + 1 >> 0];
            switch (String.fromCharCode(g)) {
            case "d":
            case "i":
            case "u":
            case "o":
            case "x":
            case "X":
            case "p":
                k = 100 == g || 105 == g;
                u = u || 4;
                var G = h = c("i" + 8 * u);
                8 == u && (h = la(h[0], h[1], 117 == g));
                4 >= u && (h = (k ? Ua : Ta)(h & Math.pow(256, u) - 1, 8 * u));
                var ja = Math.abs(h);
                k = "";
                if (100 == g || 105 == g)
                    var q = 8 == u && "object" === typeof i64Math ? i64Math.stringify(G[0], G[1], null) : Ua(h, 8 * u).toString(10);
                else if (117 == g)
                    q = 8 == u && "object" === typeof i64Math ? i64Math.stringify(G[0], G[1], !0) : Ta(h, 8 * u).toString(10),
                    h = Math.abs(h);
                else if (111 == g)
                    q = (m ? "0" : "") + ja.toString(8);
                else if (120 == g || 88 == g) {
                    k = m && 0 != h ? "0x" : "";
                    if (8 == u && "object" === typeof i64Math)
                        if (G[1]) {
                            q = (G[1] >>> 0).toString(16);
                            for (m = (G[0] >>> 0).toString(16); 8 > m.length; )
                                m = "0" + m;
                            q += m
                        } else
                            q = (G[0] >>> 0).toString(16);
                    else if (0 > h) {
                        h = -h;
                        q = (ja - 1).toString(16);
                        G = [];
                        for (m = 0; m < q.length; m++)
                            G.push((15 - parseInt(q[m], 16)).toString(16));
                        for (q = G.join(""); q.length < 2 * u; )
                            q = "f" + q
                    } else
                        q = ja.toString(16);
                    88 == g && (k = k.toUpperCase(),
                    q = q.toUpperCase())
                } else
                    112 == g && (0 === ja ? q = "(nil)" : (k = "0x",
                    q = ja.toString(16)));
                if (r)
                    for (; q.length < v; )
                        q = "0" + q;
                0 <= h && (l ? k = "+" + k : t && (k = " " + k));
                "-" == q.charAt(0) && (k = "-" + k,
                q = q.substr(1));
                for (; k.length + q.length < x; )
                    b ? q += " " : p ? q = "0" + q : k = " " + k;
                q = k + q;
                q.split("").forEach(function(a) {
                    f.push(a.charCodeAt(0))
                });
                break;
            case "f":
            case "F":
            case "e":
            case "E":
            case "g":
            case "G":
                h = c("double");
                if (isNaN(h))
                    q = "nan",
                    p = !1;
                else if (isFinite(h)) {
                    r = !1;
                    u = Math.min(v, 20);
                    if (103 == g || 71 == g)
                        r = !0,
                        v = v || 1,
                        u = parseInt(h.toExponential(u).split("e")[1], 10),
                        v > u && -4 <= u ? (g = (103 == g ? "f" : "F").charCodeAt(0),
                        v -= u + 1) : (g = (103 == g ? "e" : "E").charCodeAt(0),
                        v--),
                        u = Math.min(v, 20);
                    if (101 == g || 69 == g)
                        q = h.toExponential(u),
                        /[eE][-+]\d$/.test(q) && (q = q.slice(0, -1) + "0" + q.slice(-1));
                    else if (102 == g || 70 == g)
                        q = h.toFixed(u),
                        0 === h && (0 > h || 0 === h && -Infinity === 1 / h) && (q = "-" + q);
                    k = q.split("e");
                    if (r && !m)
                        for (; 1 < k[0].length && -1 != k[0].indexOf(".") && ("0" == k[0].slice(-1) || "." == k[0].slice(-1)); )
                            k[0] = k[0].slice(0, -1);
                    else
                        for (m && -1 == q.indexOf(".") && (k[0] += "."); v > u++; )
                            k[0] += "0";
                    q = k[0] + (1 < k.length ? "e" + k[1] : "");
                    69 == g && (q = q.toUpperCase());
                    0 <= h && (l ? q = "+" + q : t && (q = " " + q))
                } else
                    q = (0 > h ? "-" : "") + "inf",
                    p = !1;
                for (; q.length < x; )
                    q = b ? q + " " : !p || "-" != q[0] && "+" != q[0] ? (p ? "0" : " ") + q : q[0] + "0" + q.slice(1);
                97 > g && (q = q.toUpperCase());
                q.split("").forEach(function(a) {
                    f.push(a.charCodeAt(0))
                });
                break;
            case "s":
                p = (l = c("i8*")) ? Bc(l) : 6;
                r && (p = Math.min(p, v));
                if (!b)
                    for (; p < x--; )
                        f.push(32);
                if (l)
                    for (m = 0; m < p; m++)
                        f.push(H[l++ >> 0]);
                else
                    f = f.concat(Cc("(null)".substr(0, p), !0));
                if (b)
                    for (; p < x--; )
                        f.push(32);
                break;
            case "c":
                for (b && f.push(c("i8")); 0 < --x; )
                    f.push(32);
                b || f.push(c("i8"));
                break;
            case "n":
                b = c("i32*");
                B[b >> 2] = f.length;
                break;
            case "%":
                f.push(h);
                break;
            default:
                for (m = k; m < a + 2; m++)
                    f.push(F[m >> 0])
            }
            a += 2
        } else
            f.push(h),
            a += 1
    }
    return f
}
function Dc(a) {
    if (!a || !a.callee || !a.callee.name)
        return [null, "", ""];
    var b = a.callee.name, c = "(", d = !0, f;
    for (f in a) {
        var h = a[f];
        d || (c += ", ");
        d = !1;
        c = "number" === typeof h || "string" === typeof h ? c + h : c + ("(" + typeof h + ")")
    }
    c += ")";
    a = (a = a.callee.caller) ? a.arguments : [];
    d && (c = "");
    return [a, b, c]
}
function Ec(a) {
    a: {
        var b = Error();
        if (!b.stack) {
            try {
                throw Error(0);
            } catch (u) {
                b = u
            }
            if (!b.stack) {
                b = "(no stack trace available)";
                break a
            }
        }
        b = b.stack.toString()
    }
    b = b.slice(b.indexOf("\n", Math.max(b.lastIndexOf("_emscripten_log"), b.lastIndexOf("_emscripten_get_callstack"))) + 1);
    a & 8 && "undefined" === typeof emscripten_source_map && (ka || (ka = {}),
    ka['Source map information is not available, emscripten_log with EM_LOG_C_STACK will be ignored. Build with "--pre-js $EMSCRIPTEN/src/emscripten-source-map.min.js" linker flag to add source map loading to code.'] || (ka['Source map information is not available, emscripten_log with EM_LOG_C_STACK will be ignored. Build with "--pre-js $EMSCRIPTEN/src/emscripten-source-map.min.js" linker flag to add source map loading to code.'] = 1,
    e.printErr('Source map information is not available, emscripten_log with EM_LOG_C_STACK will be ignored. Build with "--pre-js $EMSCRIPTEN/src/emscripten-source-map.min.js" linker flag to add source map loading to code.')),
    a = a ^ 8 | 16);
    var c = null;
    if (a & 128)
        for (c = Dc(arguments); 0 <= c[1].indexOf("_emscripten_"); )
            c = Dc(c[0]);
    var d = b.split("\n");
    b = "";
    var f = /\s*(.*?)@(.*?):([0-9]+):([0-9]+)/, h = /\s*(.*?)@(.*):(.*)(:(.*))?/, g = /\s*at (.*?) \((.*):(.*):(.*)\)/, k;
    for (k in d) {
        var l = d[k], m;
        if ((m = g.exec(l)) && 5 == m.length) {
            l = m[1];
            var p = m[2];
            var t = m[3];
            m = m[4]
        } else if ((m = f.exec(l)) || (m = h.exec(l)),
        m && 4 <= m.length)
            l = m[1],
            p = m[2],
            t = m[3],
            m = m[4] | 0;
        else {
            b += l + "\n";
            continue
        }
        var x = l;
        x || (x = l);
        var r = !1;
        if (a & 8) {
            var v = emscripten_source_map.yc({
                line: t,
                pb: m
            });
            if (r = v && v.source)
                a & 64 && (v.source = v.source.substring(v.source.replace(/\\/g, "/").lastIndexOf("/") + 1)),
                b += "    at " + x + " (" + v.source + ":" + v.line + ":" + v.pb + ")\n"
        }
        if (a & 16 || !r)
            a & 64 && (p = p.substring(p.replace(/\\/g, "/").lastIndexOf("/") + 1)),
            b += (r ? "     = " + l : "    at " + x) + " (" + p + ":" + t + ":" + m + ")\n";
        a & 128 && c[0] && (c[1] == l && 0 < c[2].length && (b = b.replace(/\s+$/, ""),
        b += " with values: " + c[1] + c[2] + "\n"),
        c = Dc(c[0]))
    }
    return b = b.replace(/\s+$/, "")
}
var Fc = z;
z += 16;
var Gc, X = {};
function Hc(a) {
    if (0 === a)
        return 0;
    a = pa(a);
    if (!X.hasOwnProperty(a))
        return 0;
    Hc.ka && U(Hc.ka);
    a = X[a];
    var b = ta(a) + 1
      , c = E(b);
    c && sa(a, F, c, b);
    Hc.ka = c;
    return Hc.ka
}
var Y = z;
z += 48;
var Ic = na(Cc("GMT"), 2);
function Jc(a) {
    return Math.log(a) / Math.LN2
}
function Kc() {
    function a(a) {
        return (a = a.toTimeString().match(/\(([A-Za-z ]+)\)$/)) ? a[1] : "GMT"
    }
    if (!Lc) {
        Lc = !0;
        B[Mc() >> 2] = 60 * (new Date).getTimezoneOffset();
        var b = new Date(2E3,0,1)
          , c = new Date(2E3,6,1);
        B[Nc() >> 2] = Number(b.getTimezoneOffset() != c.getTimezoneOffset());
        var d = a(b)
          , f = a(c);
        d = na(Cc(d), 0);
        f = na(Cc(f), 0);
        c.getTimezoneOffset() < b.getTimezoneOffset() ? (B[Oc() >> 2] = d,
        B[Oc() + 4 >> 2] = f) : (B[Oc() >> 2] = f,
        B[Oc() + 4 >> 2] = d)
    }
}
var Lc, Pc = {}, Qc = 1;
function Rc(a, b) {
    Rc.la || (Rc.la = {});
    a in Rc.la || (e.dynCall_v(b),
    Rc.la[a] = 1)
}
function Sc(a) {
    return 0 === a % 4 && (0 !== a % 100 || 0 === a % 400)
}
function Tc(a, b) {
    for (var c = 0, d = 0; d <= b; c += a[d++])
        ;
    return c
}
var Uc = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
  , Vc = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
function Wc(a, b) {
    for (a = new Date(a.getTime()); 0 < b; ) {
        var c = a.getMonth()
          , d = (Sc(a.getFullYear()) ? Uc : Vc)[c];
        if (b > d - a.getDate())
            b -= d - a.getDate() + 1,
            a.setDate(1),
            11 > c ? a.setMonth(c + 1) : (a.setMonth(0),
            a.setFullYear(a.getFullYear() + 1));
        else {
            a.setDate(a.getDate() + b);
            break
        }
    }
    return a
}
function Xc(a, b, c, d) {
    function f(a, b, c) {
        for (a = "number" === typeof a ? a.toString() : a || ""; a.length < b; )
            a = c[0] + a;
        return a
    }
    function h(a, b) {
        return f(a, b, "0")
    }
    function g(a, b) {
        function c(a) {
            return 0 > a ? -1 : 0 < a ? 1 : 0
        }
        var d;
        0 === (d = c(a.getFullYear() - b.getFullYear())) && 0 === (d = c(a.getMonth() - b.getMonth())) && (d = c(a.getDate() - b.getDate()));
        return d
    }
    function k(a) {
        switch (a.getDay()) {
        case 0:
            return new Date(a.getFullYear() - 1,11,29);
        case 1:
            return a;
        case 2:
            return new Date(a.getFullYear(),0,3);
        case 3:
            return new Date(a.getFullYear(),0,2);
        case 4:
            return new Date(a.getFullYear(),0,1);
        case 5:
            return new Date(a.getFullYear() - 1,11,31);
        case 6:
            return new Date(a.getFullYear() - 1,11,30)
        }
    }
    function l(a) {
        a = Wc(new Date(a.j + 1900,0,1), a.Z);
        var b = k(new Date(a.getFullYear() + 1,0,4));
        return 0 >= g(k(new Date(a.getFullYear(),0,4)), a) ? 0 >= g(b, a) ? a.getFullYear() + 1 : a.getFullYear() : a.getFullYear() - 1
    }
    var m = B[d + 40 >> 2];
    d = {
        Xb: B[d >> 2],
        Wb: B[d + 4 >> 2],
        Y: B[d + 8 >> 2],
        F: B[d + 12 >> 2],
        A: B[d + 16 >> 2],
        j: B[d + 20 >> 2],
        wa: B[d + 24 >> 2],
        Z: B[d + 28 >> 2],
        Ac: B[d + 32 >> 2],
        Vb: B[d + 36 >> 2],
        Yb: m ? pa(m) : ""
    };
    c = pa(c);
    m = {
        "%c": "%a %b %d %H:%M:%S %Y",
        "%D": "%m/%d/%y",
        "%F": "%Y-%m-%d",
        "%h": "%b",
        "%r": "%I:%M:%S %p",
        "%R": "%H:%M",
        "%T": "%H:%M:%S",
        "%x": "%m/%d/%y",
        "%X": "%H:%M:%S"
    };
    for (var p in m)
        c = c.replace(new RegExp(p,"g"), m[p]);
    var t = "Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" ")
      , x = "January February March April May June July August September October November December".split(" ");
    m = {
        "%a": function(a) {
            return t[a.wa].substring(0, 3)
        },
        "%A": function(a) {
            return t[a.wa]
        },
        "%b": function(a) {
            return x[a.A].substring(0, 3)
        },
        "%B": function(a) {
            return x[a.A]
        },
        "%C": function(a) {
            return h((a.j + 1900) / 100 | 0, 2)
        },
        "%d": function(a) {
            return h(a.F, 2)
        },
        "%e": function(a) {
            return f(a.F, 2, " ")
        },
        "%g": function(a) {
            return l(a).toString().substring(2)
        },
        "%G": function(a) {
            return l(a)
        },
        "%H": function(a) {
            return h(a.Y, 2)
        },
        "%I": function(a) {
            a = a.Y;
            0 == a ? a = 12 : 12 < a && (a -= 12);
            return h(a, 2)
        },
        "%j": function(a) {
            return h(a.F + Tc(Sc(a.j + 1900) ? Uc : Vc, a.A - 1), 3)
        },
        "%m": function(a) {
            return h(a.A + 1, 2)
        },
        "%M": function(a) {
            return h(a.Wb, 2)
        },
        "%n": function() {
            return "\n"
        },
        "%p": function(a) {
            return 0 <= a.Y && 12 > a.Y ? "AM" : "PM"
        },
        "%S": function(a) {
            return h(a.Xb, 2)
        },
        "%t": function() {
            return "\t"
        },
        "%u": function(a) {
            return (new Date(a.j + 1900,a.A + 1,a.F,0,0,0,0)).getDay() || 7
        },
        "%U": function(a) {
            var b = new Date(a.j + 1900,0,1)
              , c = 0 === b.getDay() ? b : Wc(b, 7 - b.getDay());
            a = new Date(a.j + 1900,a.A,a.F);
            return 0 > g(c, a) ? h(Math.ceil((31 - c.getDate() + (Tc(Sc(a.getFullYear()) ? Uc : Vc, a.getMonth() - 1) - 31) + a.getDate()) / 7), 2) : 0 === g(c, b) ? "01" : "00"
        },
        "%V": function(a) {
            var b = k(new Date(a.j + 1900,0,4))
              , c = k(new Date(a.j + 1901,0,4))
              , d = Wc(new Date(a.j + 1900,0,1), a.Z);
            return 0 > g(d, b) ? "53" : 0 >= g(c, d) ? "01" : h(Math.ceil((b.getFullYear() < a.j + 1900 ? a.Z + 32 - b.getDate() : a.Z + 1 - b.getDate()) / 7), 2)
        },
        "%w": function(a) {
            return (new Date(a.j + 1900,a.A + 1,a.F,0,0,0,0)).getDay()
        },
        "%W": function(a) {
            var b = new Date(a.j,0,1)
              , c = 1 === b.getDay() ? b : Wc(b, 0 === b.getDay() ? 1 : 7 - b.getDay() + 1);
            a = new Date(a.j + 1900,a.A,a.F);
            return 0 > g(c, a) ? h(Math.ceil((31 - c.getDate() + (Tc(Sc(a.getFullYear()) ? Uc : Vc, a.getMonth() - 1) - 31) + a.getDate()) / 7), 2) : 0 === g(c, b) ? "01" : "00"
        },
        "%y": function(a) {
            return (a.j + 1900).toString().substring(2)
        },
        "%Y": function(a) {
            return a.j + 1900
        },
        "%z": function(a) {
            a = a.Vb;
            var b = 0 <= a;
            a = Math.abs(a) / 60;
            return (b ? "+" : "-") + String("0000" + (a / 60 * 100 + a % 60)).slice(-4)
        },
        "%Z": function(a) {
            return a.Yb
        },
        "%%": function() {
            return "%"
        }
    };
    for (p in m)
        0 <= c.indexOf(p) && (c = c.replace(new RegExp(p,"g"), m[p](d)));
    p = Cc(c, !1);
    if (p.length > b)
        return 0;
    F.set(p, a);
    return p.length - 1
}
Lb = e.InternalError = Kb("InternalError");
for (var Yc = Array(256), Zc = 0; 256 > Zc; ++Zc)
    Yc[Zc] = String.fromCharCode(Zc);
Ob = Yc;
Pb = e.BindingError = Kb("BindingError");
Q.prototype.isAliasOf = function(a) {
    if (!(this instanceof Q && a instanceof Q))
        return !1;
    var b = this.a.h.c
      , c = this.a.g
      , d = a.a.h.c;
    for (a = a.a.g; b.s; )
        c = b.R(c),
        b = b.s;
    for (; d.s; )
        a = d.R(a),
        d = d.s;
    return b === d && c === a
}
;
Q.prototype.clone = function() {
    this.a.g || Qb(this);
    if (this.a.P)
        return this.a.count.value += 1,
        this;
    var a = this.a;
    a = Object.create(Object.getPrototypeOf(this), {
        a: {
            value: {
                count: a.count,
                K: a.K,
                P: a.P,
                g: a.g,
                h: a.h,
                l: a.l,
                m: a.m
            }
        }
    });
    a.a.count.value += 1;
    a.a.K = !1;
    return a
}
;
Q.prototype["delete"] = function() {
    this.a.g || Qb(this);
    this.a.K && !this.a.P && P("Object already scheduled for deletion");
    --this.a.count.value;
    if (0 === this.a.count.value) {
        var a = this.a;
        a.l ? a.m.w(a.l) : a.h.c.w(a.g)
    }
    this.a.P || (this.a.l = void 0,
    this.a.g = void 0)
}
;
Q.prototype.isDeleted = function() {
    return !this.a.g
}
;
Q.prototype.deleteLater = function() {
    this.a.g || Qb(this);
    this.a.K && !this.a.P && P("Object already scheduled for deletion");
    Sb.push(this);
    1 === Sb.length && Rb && Rb(Tb);
    this.a.K = !0;
    return this
}
;
S.prototype.vb = function(a) {
    this.va && (a = this.va(a));
    return a
}
;
S.prototype.ta = function(a) {
    this.w && this.w(a)
}
;
S.prototype.argPackAdvance = 8;
S.prototype.readValueFromPointer = Fb;
S.prototype.deleteObject = function(a) {
    if (null !== a)
        a["delete"]()
}
;
S.prototype.fromWireType = function(a) {
    function b() {
        return this.X ? fc(this.c.L, {
            h: this.Jb,
            g: c,
            m: this,
            l: a
        }) : fc(this.c.L, {
            h: this,
            g: a
        })
    }
    var c = this.vb(a);
    if (!c)
        return this.ta(a),
        null;
    var d = ec(this.c, c);
    if (void 0 !== d) {
        if (0 === d.a.count.value)
            return d.a.g = c,
            d.a.l = a,
            d.clone();
        d = d.clone();
        this.ta(a);
        return d
    }
    d = this.c.tb(c);
    d = Ub[d];
    if (!d)
        return b.call(this);
    d = this.W ? d.qb : d.pointerType;
    var f = cc(c, this.c, d.c);
    return null === f ? b.call(this) : this.X ? fc(d.c.L, {
        h: d,
        g: f,
        m: this,
        l: a
    }) : fc(d.c.L, {
        h: d,
        g: f
    })
}
;
e.getInheritedInstanceCount = function() {
    return Object.keys(dc).length
}
;
e.getLiveInheritedInstances = function() {
    var a = [], b;
    for (b in dc)
        dc.hasOwnProperty(b) && a.push(dc[b]);
    return a
}
;
e.flushPendingDeletes = Tb;
e.setDelayFunction = function(a) {
    Rb = a;
    Sb.length && Rb && Rb(Tb)
}
;
ic = e.UnboundTypeError = Kb("UnboundTypeError");
e.count_emval_handles = function() {
    for (var a = 0, b = 5; b < V.length; ++b)
        void 0 !== V[b] && ++a;
    return a
}
;
e.get_first_emval = function() {
    for (var a = 5; a < V.length; ++a)
        if (void 0 !== V[a])
            return V[a];
    return null
}
;
zc = ba ? function() {
    var a = process.hrtime();
    return 1E3 * a[0] + a[1] / 1E6
}
: "undefined" !== typeof dateNow ? dateNow : "object" === typeof self && self.performance && "function" === typeof self.performance.now ? function() {
    return self.performance.now()
}
: "object" === typeof performance && "function" === typeof performance.now ? function() {
    return performance.now()
}
: Date.now;
var $c, Z;
Gc ? (Z = B[Fc >> 2],
$c = B[Z >> 2]) : (Gc = !0,
X.USER = X.LOGNAME = "web_user",
X.PATH = "/",
X.PWD = "/",
X.HOME = "/home/web_user",
X.LANG = "C.UTF-8",
X._ = e.thisProgram,
$c = ca(1024),
Z = ca(256),
B[Z >> 2] = $c,
B[Fc >> 2] = Z);
var ad = [], bd = 0, cd;
for (cd in X)
    if ("string" === typeof X[cd]) {
        var dd = cd + "=" + X[cd];
        ad.push(dd);
        bd += dd.length
    }
if (1024 < bd)
    throw Error("Environment size exceeded TOTAL_ENV_SIZE!");
for (var ed = 0; ed < ad.length; ed++) {
    for (var fd = dd = ad[ed], gd = $c, hd = 0; hd < fd.length; ++hd)
        F[gd++ >> 0] = fd.charCodeAt(hd);
    F[gd >> 0] = 0;
    B[Z + 4 * ed >> 2] = $c;
    $c += dd.length + 1
}
B[Z + 4 * ad.length >> 2] = 0;
A = ca(4);
Ea = Fa = ha(z);
Ga = Ea + Ka;
Ha = ha(Ga);
B[A >> 2] = Ha;
da = !0;
function Cc(a, b) {
    var c = Array(ta(a) + 1);
    a = sa(a, c, 0, c.length);
    b && (c.length = a);
    return c
}
e.wasmTableSize = 2732;
e.wasmMaxTableSize = 2732;
e.nb = {};
e.ob = {
    abort: D,
    enlargeMemory: fa,
    getTotalMemory: function() {
        return C
    },
    ___assert_fail: function(a, b, c, d) {
        D("Assertion failed: " + pa(a) + ", at: " + [b ? pa(b) : "unknown filename", c, d ? pa(d) : "unknown function"])
    },
    ___cxa_allocate_exception: function(a) {
        return E(a)
    },
    ___cxa_pure_virtual: function() {
        ma = !0;
        throw "Pure virtual function called!";
    },
    ___cxa_throw: function(a) {
        "uncaught_exception"in xb ? xb.xa++ : xb.xa = 1;
        throw a + " - Exception catching is disabled, this exception cannot be caught. Compile with -s DISABLE_EXCEPTION_CATCHING=0 or DISABLE_EXCEPTION_CATCHING=2 to catch.";
    },
    ___lock: function() {},
    ___map_file: function() {
        yb(1);
        return -1
    },
    ___setErrNo: yb,
    ___syscall140: function(a, b) {
        zb = b;
        try {
            var c = Ab.wb();
            K();
            var d = K()
              , f = K()
              , h = K();
            FS.uc(c, d, h);
            B[f >> 2] = c.position;
            c.xb && 0 === d && 0 === h && (c.xb = null);
            return 0
        } catch (g) {
            return "undefined" !== typeof FS && g instanceof FS.$ || D(g),
            -g.fa
        }
    },
    ___syscall146: Cb,
    ___syscall6: function(a, b) {
        zb = b;
        try {
            var c = Ab.wb();
            FS.close(c);
            return 0
        } catch (d) {
            return "undefined" !== typeof FS && d instanceof FS.$ || D(d),
            -d.fa
        }
    },
    ___syscall91: function(a, b) {
        zb = b;
        try {
            var c = K()
              , d = K()
              , f = Ab.Eb[c];
            if (!f)
                return 0;
            if (d === f.tc) {
                var h = FS.rc(f.qc);
                Ab.pc(c, h, d, f.flags);
                FS.xc(h);
                Ab.Eb[c] = null;
                f.nc && U(f.wc)
            }
            return 0
        } catch (g) {
            return "undefined" !== typeof FS && g instanceof FS.$ || D(g),
            -g.fa
        }
    },
    ___unlock: function() {},
    __embind_finalize_value_object: function(a) {
        var b = Db[a];
        delete Db[a];
        var c = b.ia
          , d = b.w
          , f = b.ua
          , h = f.map(function(a) {
            return a.Ab
        }).concat(f.map(function(a) {
            return a.Rb
        }));
        M([a], h, function(a) {
            var h = {};
            f.forEach(function(b, c) {
                var d = a[c]
                  , g = b.yb
                  , k = b.zb
                  , m = a[c + f.length]
                  , l = b.Qb
                  , u = b.Sb;
                h[b.sb] = {
                    read: function(a) {
                        return d.fromWireType(g(k, a))
                    },
                    write: function(a, b) {
                        var c = [];
                        l(u, a, m.toWireType(c, b));
                        Eb(c)
                    }
                }
            });
            return [{
                name: b.name,
                fromWireType: function(a) {
                    var b = {}, c;
                    for (c in h)
                        b[c] = h[c].read(a);
                    d(a);
                    return b
                },
                toWireType: function(a, b) {
                    for (var f in h)
                        if (!(f in b))
                            throw new TypeError("Missing field");
                    var g = c();
                    for (f in h)
                        h[f].write(g, b[f]);
                    null !== a && a.push(d, g);
                    return g
                },
                argPackAdvance: 8,
                readValueFromPointer: Fb,
                u: d
            }]
        })
    },
    __embind_register_bool: function(a, b, c, d, f) {
        var h = Nb(c);
        b = O(b);
        N(a, {
            name: b,
            fromWireType: function(a) {
                return !!a
            },
            toWireType: function(a, b) {
                return b ? d : f
            },
            argPackAdvance: 8,
            readValueFromPointer: function(a) {
                if (1 === c)
                    var d = F;
                else if (2 === c)
                    d = ya;
                else if (4 === c)
                    d = B;
                else
                    throw new TypeError("Unknown boolean type size: " + b);
                return this.fromWireType(d[a >> h])
            },
            u: null
        })
    },
    __embind_register_class: function(a, b, c, d, f, h, g, k, l, m, p, t, x) {
        p = O(p);
        h = T(f, h);
        k && (k = T(g, k));
        m && (m = T(l, m));
        x = T(t, x);
        var r = Ib(p);
        Wb(r, function() {
            mc("Cannot construct " + p + " due to unbound types", [d])
        });
        M([a, b, c], d ? [d] : [], function(b) {
            b = b[0];
            if (d) {
                var c = b.c;
                var f = c.L
            } else
                f = Q.prototype;
            b = Jb(r, function() {
                if (Object.getPrototypeOf(this) !== g)
                    throw new Pb("Use 'new' to construct " + p);
                if (void 0 === l.D)
                    throw new Pb(p + " has no accessible constructor");
                var a = l.D[arguments.length];
                if (void 0 === a)
                    throw new Pb("Tried to invoke ctor of " + p + " with invalid number of parameters (" + arguments.length + ") - expected (" + Object.keys(l.D).toString() + ") parameters instead!");
                return a.apply(this, arguments)
            });
            var g = Object.create(f, {
                constructor: {
                    value: b
                }
            });
            b.prototype = g;
            var l = new Xb(p,b,g,x,c,h,k,m);
            c = new S(p,l,!0,!1,!1);
            f = new S(p + "*",l,!1,!1,!1);
            var t = new S(p + " const*",l,!1,!0,!1);
            Ub[a] = {
                pointerType: f,
                qb: t
            };
            hc(r, b);
            return [c, f, t]
        })
    },
    __embind_register_class_constructor: function(a, b, c, d, f, h) {
        var g = nc(b, c);
        f = T(d, f);
        M([], [a], function(a) {
            a = a[0];
            var c = "constructor " + a.name;
            void 0 === a.c.D && (a.c.D = []);
            if (void 0 !== a.c.D[b - 1])
                throw new Pb("Cannot register multiple constructors with identical number of parameters (" + (b - 1) + ") for class '" + a.name + "'! Overload resolution is currently only performed using the parameter count, not actual type info!");
            a.c.D[b - 1] = function() {
                mc("Cannot construct " + a.name + " due to unbound types", g)
            }
            ;
            M([], g, function(d) {
                a.c.D[b - 1] = function() {
                    arguments.length !== b - 1 && P(c + " called with " + arguments.length + " arguments, expected " + (b - 1));
                    var a = []
                      , g = Array(b);
                    g[0] = h;
                    for (var k = 1; k < b; ++k)
                        g[k] = d[k].toWireType(a, arguments[k - 1]);
                    g = f.apply(null, g);
                    Eb(a);
                    return d[0].fromWireType(g)
                }
                ;
                return []
            });
            return []
        })
    },
    __embind_register_class_function: function(a, b, c, d, f, h, g, k) {
        var l = nc(c, d);
        b = O(b);
        h = T(f, h);
        M([], [a], function(a) {
            function d() {
                mc("Cannot call " + f + " due to unbound types", l)
            }
            a = a[0];
            var f = a.name + "." + b;
            k && a.c.Lb.push(b);
            var m = a.c.L
              , r = m[b];
            void 0 === r || void 0 === r.v && r.className !== a.name && r.U === c - 2 ? (d.U = c - 2,
            d.className = a.name,
            m[b] = d) : (Vb(m, b, f),
            m[b].v[c - 2] = d);
            M([], l, function(d) {
                var k = f
                  , l = a
                  , p = h
                  , q = d.length;
                2 > q && P("argTypes array size mismatch! Must at least get return value and 'this' types!");
                var r = null !== d[1] && null !== l
                  , t = !1;
                for (l = 1; l < d.length; ++l)
                    if (null !== d[l] && void 0 === d[l].u) {
                        t = !0;
                        break
                    }
                var x = "void" !== d[0].name
                  , v = ""
                  , ia = "";
                for (l = 0; l < q - 2; ++l)
                    v += (0 !== l ? ", " : "") + "arg" + l,
                    ia += (0 !== l ? ", " : "") + "arg" + l + "Wired";
                k = "return function " + Ib(k) + "(" + v + ") {\nif (arguments.length !== " + (q - 2) + ") {\nthrowBindingError('function " + k + " called with ' + arguments.length + ' arguments, expected " + (q - 2) + " args!');\n}\n";
                t && (k += "var destructors = [];\n");
                var jc = t ? "destructors" : "null";
                v = "throwBindingError invoker fn runDestructors retType classParam".split(" ");
                p = [P, p, g, Eb, d[0], d[1]];
                r && (k += "var thisWired = classParam.toWireType(" + jc + ", this);\n");
                for (l = 0; l < q - 2; ++l)
                    k += "var arg" + l + "Wired = argType" + l + ".toWireType(" + jc + ", arg" + l + "); // " + d[l + 2].name + "\n",
                    v.push("argType" + l),
                    p.push(d[l + 2]);
                r && (ia = "thisWired" + (0 < ia.length ? ", " : "") + ia);
                k += (x ? "var rv = " : "") + "invoker(fn" + (0 < ia.length ? ", " : "") + ia + ");\n";
                if (t)
                    k += "runDestructors(destructors);\n";
                else
                    for (l = r ? 1 : 2; l < d.length; ++l)
                        q = 1 === l ? "thisWired" : "arg" + (l - 2) + "Wired",
                        null !== d[l].u && (k += q + "_dtor(" + q + "); // " + d[l].name + "\n",
                        v.push(q + "_dtor"),
                        p.push(d[l].u));
                x && (k += "var ret = retType.fromWireType(rv);\nreturn ret;\n");
                v.push(k + "}\n");
                d = oc(v).apply(null, p);
                void 0 === m[b].v ? (d.U = c - 2,
                m[b] = d) : m[b].v[c - 2] = d;
                return []
            });
            return []
        })
    },
    __embind_register_emval: function(a, b) {
        b = O(b);
        N(a, {
            name: b,
            fromWireType: function(a) {
                var b = V[a].value;
                qc(a);
                return b
            },
            toWireType: function(a, b) {
                return R(b)
            },
            argPackAdvance: 8,
            readValueFromPointer: Fb,
            u: null
        })
    },
    __embind_register_float: function(a, b, c) {
        c = Nb(c);
        b = O(b);
        N(a, {
            name: b,
            fromWireType: function(a) {
                return a
            },
            toWireType: function(a, b) {
                if ("number" !== typeof b && "boolean" !== typeof b)
                    throw new TypeError('Cannot convert "' + $b(b) + '" to ' + this.name);
                return b
            },
            argPackAdvance: 8,
            readValueFromPointer: rc(b, c),
            u: null
        })
    },
    __embind_register_integer: function(a, b, c, d, f) {
        function h(a) {
            return a
        }
        b = O(b);
        -1 === f && (f = 4294967295);
        var g = Nb(c);
        if (0 === d) {
            var k = 32 - 8 * c;
            h = function(a) {
                return a << k >>> k
            }
        }
        var l = -1 != b.indexOf("unsigned");
        N(a, {
            name: b,
            fromWireType: h,
            toWireType: function(a, c) {
                if ("number" !== typeof c && "boolean" !== typeof c)
                    throw new TypeError('Cannot convert "' + $b(c) + '" to ' + this.name);
                if (c < d || c > f)
                    throw new TypeError('Passing a number "' + $b(c) + '" from JS side to C/C++ side to an argument of type "' + b + '", which is outside the valid range [' + d + ", " + f + "]!");
                return l ? c >>> 0 : c | 0
            },
            argPackAdvance: 8,
            readValueFromPointer: sc(b, g, 0 !== d),
            u: null
        })
    },
    __embind_register_memory_view: function(a, b, c) {
        function d(a) {
            a >>= 2;
            var b = I;
            return new f(b.buffer,b[a + 1],b[a])
        }
        var f = [Int8Array, Uint8Array, Int16Array, Uint16Array, Int32Array, Uint32Array, Float32Array, Float64Array][b];
        c = O(c);
        N(a, {
            name: c,
            fromWireType: d,
            argPackAdvance: 8,
            readValueFromPointer: d
        }, {
            Bb: !0
        })
    },
    __embind_register_smart_ptr: function(a, b, c, d, f, h, g, k, l, m, p, t) {
        c = O(c);
        h = T(f, h);
        k = T(g, k);
        m = T(l, m);
        t = T(p, t);
        M([a], [b], function(a) {
            a = a[0];
            return [new S(c,a.c,!1,!1,!0,a,d,h,k,m,t)]
        })
    },
    __embind_register_std_string: function(a, b) {
        b = O(b);
        N(a, {
            name: b,
            fromWireType: function(a) {
                for (var b = I[a >> 2], c = Array(b), h = 0; h < b; ++h)
                    c[h] = String.fromCharCode(H[a + 4 + h]);
                U(a);
                return c.join("")
            },
            toWireType: function(a, b) {
                function c(a, b) {
                    return a[b]
                }
                function d(a, b) {
                    return a.charCodeAt(b)
                }
                b instanceof ArrayBuffer && (b = new Uint8Array(b));
                var g;
                b instanceof Uint8Array ? g = c : b instanceof Uint8ClampedArray ? g = c : b instanceof Int8Array ? g = c : "string" === typeof b ? g = d : P("Cannot pass non-string to std::string");
                var k = b.length
                  , l = E(4 + k);
                I[l >> 2] = k;
                for (var m = 0; m < k; ++m) {
                    var p = g(b, m);
                    255 < p && (U(l),
                    P("String has UTF-16 code units that do not fit in 8 bits"));
                    H[l + 4 + m] = p
                }
                null !== a && a.push(U, l);
                return l
            },
            argPackAdvance: 8,
            readValueFromPointer: Fb,
            u: function(a) {
                U(a)
            }
        })
    },
    __embind_register_std_wstring: function(a, b, c) {
        c = O(c);
        if (2 === b) {
            var d = function() {
                return za
            };
            var f = 1
        } else
            4 === b && (d = function() {
                return I
            }
            ,
            f = 2);
        N(a, {
            name: c,
            fromWireType: function(a) {
                for (var b = d(), c = I[a >> 2], h = Array(c), m = a + 4 >> f, p = 0; p < c; ++p)
                    h[p] = String.fromCharCode(b[m + p]);
                U(a);
                return h.join("")
            },
            toWireType: function(a, c) {
                var h = d()
                  , g = c.length
                  , m = E(4 + g * b);
                I[m >> 2] = g;
                for (var p = m + 4 >> f, t = 0; t < g; ++t)
                    h[p + t] = c.charCodeAt(t);
                null !== a && a.push(U, m);
                return m
            },
            argPackAdvance: 8,
            readValueFromPointer: Fb,
            u: function(a) {
                U(a)
            }
        })
    },
    __embind_register_value_object: function(a, b, c, d, f, h) {
        Db[a] = {
            name: O(b),
            ia: T(c, d),
            w: T(f, h),
            ua: []
        }
    },
    __embind_register_value_object_field: function(a, b, c, d, f, h, g, k, l, m) {
        Db[a].ua.push({
            sb: O(b),
            Ab: c,
            yb: T(d, f),
            zb: h,
            Rb: g,
            Qb: T(k, l),
            Sb: m
        })
    },
    __embind_register_void: function(a, b) {
        b = O(b);
        N(a, {
            Cb: !0,
            name: b,
            argPackAdvance: 0,
            fromWireType: function() {},
            toWireType: function() {}
        })
    },
    __emval_as: function(a, b, c) {
        a = W(a);
        b = tc(b, "emval::as");
        var d = []
          , f = R(d);
        B[c >> 2] = f;
        return b.toWireType(d, a)
    },
    __emval_call: function(a, b, c, d) {
        a = W(a);
        c = uc(b, c);
        for (var f = Array(b), h = 0; h < b; ++h) {
            var g = c[h];
            f[h] = g.readValueFromPointer(d);
            d += g.argPackAdvance
        }
        a = a.apply(void 0, f);
        return R(a)
    },
    __emval_call_method: function(a, b, c, d, f) {
        a = xc[a];
        b = W(b);
        c = wc(c);
        var h = [];
        B[d >> 2] = R(h);
        return a(b, c, h, f)
    },
    __emval_call_void_method: function(a, b, c, d) {
        a = xc[a];
        b = W(b);
        c = wc(c);
        a(b, c, null, d)
    },
    __emval_decref: qc,
    __emval_equals: function(a, b) {
        a = W(a);
        b = W(b);
        return a == b
    },
    __emval_get_global: function(a) {
        if (0 === a)
            return R(Function("return this")());
        a = wc(a);
        return R(Function("return this")()[a])
    },
    __emval_get_method_caller: function(a, b) {
        b = uc(a, b);
        for (var c = b[0], d = c.name + "_$" + b.slice(1).map(function(a) {
            return a.name
        }).join("_") + "$", f = ["retType"], h = [c], g = "", k = 0; k < a - 1; ++k)
            g += (0 !== k ? ", " : "") + "arg" + k,
            f.push("argType" + k),
            h.push(b[1 + k]);
        d = "return function " + Ib("methodCaller_" + d) + "(handle, name, destructors, args) {\n";
        var l = 0;
        for (k = 0; k < a - 1; ++k)
            d += "    var arg" + k + " = argType" + k + ".readValueFromPointer(args" + (l ? "+" + l : "") + ");\n",
            l += b[k + 1].argPackAdvance;
        d += "    var rv = handle[name](" + g + ");\n";
        for (k = 0; k < a - 1; ++k)
            b[k + 1].deleteObject && (d += "    argType" + k + ".deleteObject(arg" + k + ");\n");
        c.Cb || (d += "    return retType.toWireType(destructors, rv);\n");
        f.push(d + "};\n");
        a = oc(f).apply(null, h);
        return yc(a)
    },
    __emval_get_module_property: function(a) {
        a = wc(a);
        return R(e[a])
    },
    __emval_get_property: function(a, b) {
        a = W(a);
        b = W(b);
        return R(a[b])
    },
    __emval_incref: function(a) {
        4 < a && (V[a].ja += 1)
    },
    __emval_new_array: function() {
        return R([])
    },
    __emval_new_cstring: function(a) {
        return R(wc(a))
    },
    __emval_new_object: function() {
        return R({})
    },
    __emval_run_destructors: function(a) {
        Eb(V[a].value);
        qc(a)
    },
    __emval_set_property: function(a, b, c) {
        a = W(a);
        b = W(b);
        c = W(c);
        a[b] = c
    },
    __emval_take_value: function(a, b) {
        a = tc(a, "_emval_take_value");
        a = a.readValueFromPointer(b);
        return R(a)
    },
    __emval_typeof: function(a) {
        a = W(a);
        return R(typeof a)
    },
    _abort: function() {
        e.abort()
    },
    _clock_gettime: function(a, b) {
        if (0 === a)
            a = Date.now();
        else if (1 === a && (ba || "undefined" !== typeof dateNow || (w || y) && self.performance && self.performance.now))
            a = zc();
        else
            return yb(22),
            -1;
        B[b >> 2] = a / 1E3 | 0;
        B[b + 4 >> 2] = a % 1E3 * 1E6 | 0;
        return 0
    },
    _emscripten_log: function(a, b) {
        var c = B[b >> 2];
        b += 4;
        var d = "";
        if (c)
            for (b = Ac(c, b),
            c = 0; c < b.length; ++c)
                d += String.fromCharCode(b[c]);
        a & 24 && (d = d.replace(/\s+$/, ""),
        d += (0 < d.length ? "\n" : "") + Ec(a));
        a & 1 ? a & 4 ? console.error(d) : a & 2 ? console.warn(d) : console.log(d) : a & 6 ? e.printErr(d) : e.print(d)
    },
    _emscripten_memcpy_big: function(a, b, c) {
        H.set(H.subarray(b, b + c), a);
        return a
    },
    _getenv: Hc,
    _gmtime: function(a) {
        a = new Date(1E3 * B[a >> 2]);
        B[Y >> 2] = a.getUTCSeconds();
        B[Y + 4 >> 2] = a.getUTCMinutes();
        B[Y + 8 >> 2] = a.getUTCHours();
        B[Y + 12 >> 2] = a.getUTCDate();
        B[Y + 16 >> 2] = a.getUTCMonth();
        B[Y + 20 >> 2] = a.getUTCFullYear() - 1900;
        B[Y + 24 >> 2] = a.getUTCDay();
        B[Y + 36 >> 2] = 0;
        B[Y + 32 >> 2] = 0;
        B[Y + 28 >> 2] = (a.getTime() - Date.UTC(a.getUTCFullYear(), 0, 1, 0, 0, 0, 0)) / 864E5 | 0;
        B[Y + 40 >> 2] = Ic;
        return Y
    },
    _llvm_log2_f64: function() {
        return Jc.apply(null, arguments)
    },
    _llvm_trap: function() {
        D("trap!")
    },
    _localtime: function(a) {
        Kc();
        a = new Date(1E3 * B[a >> 2]);
        B[Y >> 2] = a.getSeconds();
        B[Y + 4 >> 2] = a.getMinutes();
        B[Y + 8 >> 2] = a.getHours();
        B[Y + 12 >> 2] = a.getDate();
        B[Y + 16 >> 2] = a.getMonth();
        B[Y + 20 >> 2] = a.getFullYear() - 1900;
        B[Y + 24 >> 2] = a.getDay();
        var b = new Date(a.getFullYear(),0,1);
        B[Y + 28 >> 2] = (a.getTime() - b.getTime()) / 864E5 | 0;
        B[Y + 36 >> 2] = -(60 * a.getTimezoneOffset());
        var c = (new Date(2E3,6,1)).getTimezoneOffset();
        b = b.getTimezoneOffset();
        a = (c != b && a.getTimezoneOffset() == Math.min(b, c)) | 0;
        B[Y + 32 >> 2] = a;
        a = B[Oc() + (a ? 4 : 0) >> 2];
        B[Y + 40 >> 2] = a;
        return Y
    },
    _mktime: function(a) {
        Kc();
        var b = new Date(B[a + 20 >> 2] + 1900,B[a + 16 >> 2],B[a + 12 >> 2],B[a + 8 >> 2],B[a + 4 >> 2],B[a >> 2],0)
          , c = B[a + 32 >> 2]
          , d = b.getTimezoneOffset()
          , f = new Date(b.getFullYear(),0,1)
          , h = (new Date(2E3,6,1)).getTimezoneOffset()
          , g = f.getTimezoneOffset()
          , k = Math.min(g, h);
        0 > c ? B[a + 32 >> 2] = Number(h != g && k == d) : 0 < c != (k == d) && (h = Math.max(g, h),
        b.setTime(b.getTime() + 6E4 * ((0 < c ? k : h) - d)));
        B[a + 24 >> 2] = b.getDay();
        B[a + 28 >> 2] = (b.getTime() - f.getTime()) / 864E5 | 0;
        return b.getTime() / 1E3 | 0
    },
    _pthread_cond_wait: function() {
        return 0
    },
    _pthread_getspecific: function(a) {
        return Pc[a] || 0
    },
    _pthread_key_create: function(a) {
        if (0 == a)
            return 22;
        B[a >> 2] = Qc;
        Pc[Qc] = 0;
        Qc++;
        return 0
    },
    _pthread_mutex_destroy: function() {},
    _pthread_mutex_init: function() {},
    _pthread_mutexattr_destroy: function() {},
    _pthread_mutexattr_init: function() {},
    _pthread_mutexattr_settype: function() {},
    _pthread_once: Rc,
    _pthread_setspecific: function(a, b) {
        if (!(a in Pc))
            return 22;
        Pc[a] = b;
        return 0
    },
    _strftime_l: function(a, b, c, d) {
        return Xc(a, b, c, d)
    },
    _time: function(a) {
        var b = Date.now() / 1E3 | 0;
        a && (B[a >> 2] = b);
        return b
    },
    DYNAMICTOP_PTR: A,
    STACKTOP: Fa
};
var id = e.asm(e.nb, e.ob, buffer);
e.asm = id;
var bb = e.__GLOBAL__sub_I_aacutil_cpp = function() {
    return e.asm.__GLOBAL__sub_I_aacutil_cpp.apply(null, arguments)
}
  , qb = e.__GLOBAL__sub_I_analyticstracker_cpp = function() {
    return e.asm.__GLOBAL__sub_I_analyticstracker_cpp.apply(null, arguments)
}
  , kb = e.__GLOBAL__sub_I_bandwidthfilter_cpp = function() {
    return e.asm.__GLOBAL__sub_I_bandwidthfilter_cpp.apply(null, arguments)
}
  , $a = e.__GLOBAL__sub_I_bind_cpp = function() {
    return e.asm.__GLOBAL__sub_I_bind_cpp.apply(null, arguments)
}
  , jb = e.__GLOBAL__sub_I_bitratefilter_cpp = function() {
    return e.asm.__GLOBAL__sub_I_bitratefilter_cpp.apply(null, arguments)
}
  , ib = e.__GLOBAL__sub_I_bufferfilter_cpp = function() {
    return e.asm.__GLOBAL__sub_I_bufferfilter_cpp.apply(null, arguments)
}
  , fb = e.__GLOBAL__sub_I_channelsource_cpp = function() {
    return e.asm.__GLOBAL__sub_I_channelsource_cpp.apply(null, arguments)
}
  , sb = e.__GLOBAL__sub_I_codecstring_cpp = function() {
    return e.asm.__GLOBAL__sub_I_codecstring_cpp.apply(null, arguments)
}
  , hb = e.__GLOBAL__sub_I_droppedframefilter_cpp = function() {
    return e.asm.__GLOBAL__sub_I_droppedframefilter_cpp.apply(null, arguments)
}
  , db = e.__GLOBAL__sub_I_masterplaylist_cpp = function() {
    return e.asm.__GLOBAL__sub_I_masterplaylist_cpp.apply(null, arguments)
}
  , gb = e.__GLOBAL__sub_I_maxbufferfilter_cpp = function() {
    return e.asm.__GLOBAL__sub_I_maxbufferfilter_cpp.apply(null, arguments)
}
  , ub = e.__GLOBAL__sub_I_mediaplayer_cpp = function() {
    return e.asm.__GLOBAL__sub_I_mediaplayer_cpp.apply(null, arguments)
}
  , cb = e.__GLOBAL__sub_I_mediaplaylist_cpp = function() {
    return e.asm.__GLOBAL__sub_I_mediaplaylist_cpp.apply(null, arguments)
}
  , tb = e.__GLOBAL__sub_I_mediarequest_cpp = function() {
    return e.asm.__GLOBAL__sub_I_mediarequest_cpp.apply(null, arguments)
}
  , rb = e.__GLOBAL__sub_I_mediatype_cpp = function() {
    return e.asm.__GLOBAL__sub_I_mediatype_cpp.apply(null, arguments)
}
  , pb = e.__GLOBAL__sub_I_minutewatched_cpp = function() {
    return e.asm.__GLOBAL__sub_I_minutewatched_cpp.apply(null, arguments)
}
  , ob = e.__GLOBAL__sub_I_qualitychanged_cpp = function() {
    return e.asm.__GLOBAL__sub_I_qualitychanged_cpp.apply(null, arguments)
}
  , ab = e.__GLOBAL__sub_I_random_cpp = function() {
    return e.asm.__GLOBAL__sub_I_random_cpp.apply(null, arguments)
}
  , mb = e.__GLOBAL__sub_I_resolutionfilter_cpp = function() {
    return e.asm.__GLOBAL__sub_I_resolutionfilter_cpp.apply(null, arguments)
}
  , nb = e.__GLOBAL__sub_I_spadeclient_cpp = function() {
    return e.asm.__GLOBAL__sub_I_spadeclient_cpp.apply(null, arguments)
}
  , eb = e.__GLOBAL__sub_I_twitchapi_cpp = function() {
    return e.asm.__GLOBAL__sub_I_twitchapi_cpp.apply(null, arguments)
}
  , Za = e.__GLOBAL__sub_I_viewportfilter_cpp = function() {
    return e.asm.__GLOBAL__sub_I_viewportfilter_cpp.apply(null, arguments)
}
  , vb = e.__GLOBAL__sub_I_web_http_cpp = function() {
    return e.asm.__GLOBAL__sub_I_web_http_cpp.apply(null, arguments)
}
  , lb = e.__GLOBAL__sub_I_web_mediaplayer_cpp = function() {
    return e.asm.__GLOBAL__sub_I_web_mediaplayer_cpp.apply(null, arguments)
}
  , wb = e.__GLOBAL__sub_I_web_scheduler_cpp = function() {
    return e.asm.__GLOBAL__sub_I_web_scheduler_cpp.apply(null, arguments)
}
  , lc = e.___getTypeName = function() {
    return e.asm.___getTypeName.apply(null, arguments)
}
  , Nc = e.__get_daylight = function() {
    return e.asm.__get_daylight.apply(null, arguments)
}
  , Mc = e.__get_timezone = function() {
    return e.asm.__get_timezone.apply(null, arguments)
}
  , Oc = e.__get_tzname = function() {
    return e.asm.__get_tzname.apply(null, arguments)
}
  , Ia = e._emscripten_replace_memory = function() {
    return e.asm._emscripten_replace_memory.apply(null, arguments)
}
  , U = e._free = function() {
    return e.asm._free.apply(null, arguments)
}
  , E = e._malloc = function() {
    return e.asm._malloc.apply(null, arguments)
}
  , Bc = e._strlen = function() {
    return e.asm._strlen.apply(null, arguments)
}
  , oa = e.stackAlloc = function() {
    return e.asm.stackAlloc.apply(null, arguments)
}
;
e.dynCall_di = function() {
    return e.asm.dynCall_di.apply(null, arguments)
}
;
e.dynCall_fi = function() {
    return e.asm.dynCall_fi.apply(null, arguments)
}
;
e.dynCall_fii = function() {
    return e.asm.dynCall_fii.apply(null, arguments)
}
;
e.dynCall_i = function() {
    return e.asm.dynCall_i.apply(null, arguments)
}
;
e.dynCall_ii = function() {
    return e.asm.dynCall_ii.apply(null, arguments)
}
;
e.dynCall_iii = function() {
    return e.asm.dynCall_iii.apply(null, arguments)
}
;
e.dynCall_iiii = function() {
    return e.asm.dynCall_iiii.apply(null, arguments)
}
;
e.dynCall_iiiii = function() {
    return e.asm.dynCall_iiiii.apply(null, arguments)
}
;
e.dynCall_iiiiid = function() {
    return e.asm.dynCall_iiiiid.apply(null, arguments)
}
;
e.dynCall_iiiiii = function() {
    return e.asm.dynCall_iiiiii.apply(null, arguments)
}
;
e.dynCall_iiiiiid = function() {
    return e.asm.dynCall_iiiiiid.apply(null, arguments)
}
;
e.dynCall_iiiiiii = function() {
    return e.asm.dynCall_iiiiiii.apply(null, arguments)
}
;
e.dynCall_iiiiiiii = function() {
    return e.asm.dynCall_iiiiiiii.apply(null, arguments)
}
;
e.dynCall_iiiiiiiii = function() {
    return e.asm.dynCall_iiiiiiiii.apply(null, arguments)
}
;
e.dynCall_iiiiij = function() {
    return e.asm.dynCall_iiiiij.apply(null, arguments)
}
;
e.dynCall_iijiiiii = function() {
    return e.asm.dynCall_iijiiiii.apply(null, arguments)
}
;
e.dynCall_ji = function() {
    return e.asm.dynCall_ji.apply(null, arguments)
}
;
e.dynCall_jiii = function() {
    return e.asm.dynCall_jiii.apply(null, arguments)
}
;
e.dynCall_v = function() {
    return e.asm.dynCall_v.apply(null, arguments)
}
;
e.dynCall_vi = function() {
    return e.asm.dynCall_vi.apply(null, arguments)
}
;
e.dynCall_vid = function() {
    return e.asm.dynCall_vid.apply(null, arguments)
}
;
e.dynCall_vif = function() {
    return e.asm.dynCall_vif.apply(null, arguments)
}
;
e.dynCall_vii = function() {
    return e.asm.dynCall_vii.apply(null, arguments)
}
;
e.dynCall_viid = function() {
    return e.asm.dynCall_viid.apply(null, arguments)
}
;
e.dynCall_viif = function() {
    return e.asm.dynCall_viif.apply(null, arguments)
}
;
e.dynCall_viii = function() {
    return e.asm.dynCall_viii.apply(null, arguments)
}
;
e.dynCall_viiii = function() {
    return e.asm.dynCall_viiii.apply(null, arguments)
}
;
e.dynCall_viiiii = function() {
    return e.asm.dynCall_viiiii.apply(null, arguments)
}
;
e.dynCall_viiiiii = function() {
    return e.asm.dynCall_viiiiii.apply(null, arguments)
}
;
e.dynCall_vij = function() {
    return e.asm.dynCall_vij.apply(null, arguments)
}
;
e.dynCall_vijii = function() {
    return e.asm.dynCall_vijii.apply(null, arguments)
}
;
e.asm = id;
function jd(a) {
    this.name = "ExitStatus";
    this.message = "Program terminated with exit(" + a + ")";
    this.status = a
}
jd.prototype = Error();
jd.prototype.constructor = jd;
Wa = function kd() {
    e.calledRun || ld();
    e.calledRun || (Wa = kd)
}
;
function ld() {
    function a() {
        if (!e.calledRun && (e.calledRun = !0,
        !ma)) {
            Ra || (Ra = !0,
            La(Na));
            La(Oa);
            if (e.onRuntimeInitialized)
                e.onRuntimeInitialized();
            if (e.postRun)
                for ("function" == typeof e.postRun && (e.postRun = [e.postRun]); e.postRun.length; ) {
                    var a = e.postRun.shift();
                    Qa.unshift(a)
                }
            La(Qa)
        }
    }
    if (!(0 < J)) {
        if (e.preRun)
            for ("function" == typeof e.preRun && (e.preRun = [e.preRun]); e.preRun.length; )
                Sa();
        La(Ma);
        0 < J || e.calledRun || (e.setStatus ? (e.setStatus("Running..."),
        setTimeout(function() {
            setTimeout(function() {
                e.setStatus("")
            }, 1);
            a()
        }, 1)) : a())
    }
}
e.run = ld;
e.exit = function(a, b) {
    if (!b || !e.noExitRuntime || 0 !== a) {
        if (!e.noExitRuntime && (ma = !0,
        Fa = void 0,
        La(Pa),
        e.onExit))
            e.onExit(a);
        ba && process.exit(a);
        e.quit(a, new jd(a))
    }
}
;
function D(a) {
    if (e.onAbort)
        e.onAbort(a);
    void 0 !== a ? (e.print(a),
    e.printErr(a),
    a = JSON.stringify(a)) : a = "";
    ma = !0;
    throw "abort(" + a + "). Build with -s ASSERTIONS=1 for more info.";
}
e.abort = D;
if (e.preInit)
    for ("function" == typeof e.preInit && (e.preInit = [e.preInit]); 0 < e.preInit.length; )
        e.preInit.pop()();
e.noExitRuntime = !0;
ld();
